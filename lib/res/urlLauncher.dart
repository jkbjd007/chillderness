import 'package:url_launcher/url_launcher.dart';
class UrlLaunch{
  static BrowseLocalOffers() async   {
    String url = "https://www.chillderness.co.uk/offers";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("Error in Url ${url}" );
      throw 'Could not launch $url';
    }
  }


  static VisittheInstagram() async {
    String url = "https://www.instagram.com/chilldernessretreats";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("Error in Url ${url}" );
      throw 'Could not launch $url';
    }
  }

  static Reviewus() async {
    String url = "https://www.chillderness.co.uk/reviewus";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("Error in Url ${url}" );
      throw 'Could not launch $url';
    }
  }

  static Viewtheguide() async {
    String url = "https://docs.google.com/presentation/d/1Kt1blGup2EyBtgxEDJYjkwLRnpqfR0GDzwGYeKlGT6Q/edit#slide=id.p";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("Error in Url ${url}" );
      throw 'Could not launch $url';
    }
  }

  static OpenGoogleMap(String urls) async {
    String url = urls;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("Error in Url ${url}" );
      throw 'Could not launch $url';
    }
  }
}