import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';

class styles{
  static TextStyle PlayfairDisplayBoldItalic ({double fontSize = 23 , String fontFamily = "PlayfairDisplay-BoldItalic", Color  color = Colors.black}){
    return TextStyle(
    fontFamily: fontFamily,
    fontSize: fontSize,
color: color
);
}

  static TextStyle PlayfairDisplayBlack ({double fontSize = 14 , String fontFamily = "PlayfairDisplay-Black", Color  color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle QuicksandBold({double fontSize = 18 , String fontFamily = "Quicksand-Bold", Color  color}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color?? detailTextColor,
    );
  }

  static TextStyle QuicksandLight({double fontSize = 14 , String fontFamily = "Quicksand-Light", Color  color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle HelveticaNeue({double fontSize = 20 , String fontFamily = "HelveticaNeue",Color color}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle HelveticaNeueBd({double fontSize = 20 , String fontFamily = "HelveticaNeueBd", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle RobotoCondensedBold({double fontSize = 18 , String fontFamily = "RobotoCondensed-Boldr", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle RobotoMedium({double fontSize = 10 , String fontFamily = "RobotoMedium", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
}

