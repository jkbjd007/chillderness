import 'package:Chillderness/res/color.dart';
import 'package:Chillderness/res/urlLauncher.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class beginingNavigation extends StatefulWidget {
  @override
  _beginingNavigationState createState() => _beginingNavigationState();
}

class _beginingNavigationState extends State<beginingNavigation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Navigating to the Conker:",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 22)
                          )
                      ),
                    ),
                  ),
                ],
              ),),

            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "First open Google Map and\nnavigate to Newbridge on Wye",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 22)
                          )
                      ),
                    ),
                  ),
                ],
              ),),

            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Once you have arrived please click the following button.",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 22)
                          )
                      ),
                    ),
                  ),
                ],
              ),),

            SizedBox(height: size.convert(context, 25),),
            GestureDetector(
              onTap: (){
                UrlLaunch.OpenGoogleMap("https://goo.gl/maps/HAAhMZm9dQF1EViD8");
//                Navigator.push(context, PageTransition(child: beginingNavigation(),
//                    type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "I have arrived at Newbridge on Wye",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),


            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "(This is due to google maps going the wrong way when setting navigation directly to conker)",
                          style: styles.HelveticaNeue(
                              fontSize: size.convert(context, 20),
                            color: Colors.black
                          )
                      ),
                    ),
                  ),
                ],
              ),),

            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/map1.png",),),

//            SizedBox(height: size.convert(context, 10),),
//            Container(
//              height: size.convert(context, 500),
//              padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 15)),
//              decoration: BoxDecoration(
//                color: boxColor,
//                borderRadius: BorderRadius.circular(10)
//              ),
//              margin: EdgeInsets.symmetric(horizontal: 35),
//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    child: RichText(
//                      text: TextSpan(
//                          text: "Mapbox navigation integrated here,\n"
//                      "alternatively this page could\n"
//              "redirect straight\n"
//              "to google navigation but I would\n"
//                  "prefer the first option,",
//                          style: styles.PlayfairDisplayBoldItalic(
//                              fontSize: size.convert(context, 15),
//                            color: boxTextColor
//                          )
//                      ),
//                    ),
//                  ),
//                ],
//              ),),

          ],
        ),),
      ),
    );
  }
}
