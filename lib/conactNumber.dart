import 'package:Chillderness/otherScreen/Late-night-arrivals.dart';
import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class conactNumber extends StatefulWidget {

  @override
  _conactNumberState createState() => _conactNumberState();
}

class _conactNumberState extends State<conactNumber> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Contact Numbers",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: 28
                          )
                      ),
                    ),
                  ),
                ],
              ),),

            SizedBox(height: size.convert(context, 20),),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15,
              vertical: 10),
              width: size.size362(context, 362),
              height: 310,
              margin: EdgeInsets.symmetric(horizontal: 35),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  width: 2,
                  color: Color(0xffFF0000)
                ),
              ),
              child: Column(
                children: <Widget>[
                  Row(
                      children: <Widget>[
                        Expanded(
                          child: RichText(
                            text: TextSpan(
                                text: "Wales:",
                                style: styles.PlayfairDisplayBoldItalic(
                                    fontSize: 28,
                                  color: Color(0xffFF0000)
                                )
                            ),
                          ),
                        ),
                      ],
                    ),
                  SizedBox(height: 15,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: RichText(
                          text: TextSpan(
                              text: "In an emergency always call 999. If you have a medical query, its 111.",
                              style: styles.HelveticaNeueBd(
                                  fontSize: 23,
                                  color: Colors.black
                              )
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 30,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: RichText(
                          text: TextSpan(
                              text: "Contact Chillderness:"
                                    "\n+44 7951 957067"
                                    "\nOr"
                                    "\nbookings@chillderness.co.uk",
                              style: styles.HelveticaNeueBd(
                                  fontSize: 23,
                                  color: Colors.black
                              )
                          ),
                        ),
                      ),
                    ],
                  ),

                ],
              ),),

            SizedBox(height: size.convert(context, 20),),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15,
                  vertical: 10),
              width: size.size362(context, 362),
              height: 310,
              margin: EdgeInsets.symmetric(horizontal: 35),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                    width: 2,
                    color: Color(0xff0058FF)
                ),
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: RichText(
                          text: TextSpan(
                              text: "France:",
                              style: styles.PlayfairDisplayBoldItalic(
                                  fontSize: 28,
                                  color: Color(0xff0058FF)
                              )
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: RichText(
                          text: TextSpan(
                              text: "In an emergency always call 112.",
                              style: styles.HelveticaNeueBd(
                                  fontSize: 23,
                                  color: Colors.black
                              )
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 30,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: RichText(
                          text: TextSpan(
                              text: "Contact Chillderness:"
                                  "\n+44 7951 957067"
                                  "\nOr"
                                  "\nbookings@chillderness.co.uk",
                              style: styles.HelveticaNeueBd(
                                  fontSize: 23,
                                  color: Colors.black
                              )
                          ),
                        ),
                      ),
                    ],
                  ),

                ],
              ),),

          ],
        ),),
      ),
    );
  }
}
