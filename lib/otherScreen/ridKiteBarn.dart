import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';

class ridKiteBarn extends StatefulWidget {
  @override
  _ridKiteBarnState createState() => _ridKiteBarnState();
}

class _ridKiteBarnState extends State<ridKiteBarn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Red Kite Barn",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 19)
                          )
                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 5),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "Our Guide",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 15,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/ridKitBarn/ridKitBarnMenu.png",),),
            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                            children: <TextSpan>[

                              TextSpan(
                                text: "\nYour adventure begins at 4pm."
                                    "\n\nWe keep the key in a key vault and will let you know the secret password upon confirmation of booking. It is really important that whenever you leave the barn the front door key is re-placed in the key vault. It is all too easy to pull the front door shut with the key on the inside of the barn, so please do take particular care."
                                    "\n\nWhen you get in the house… you will notice that it is an upside down barn: bedrooms and bathrooms on the ground floor, kitchen and living space upstairs."
                                    "\n\nLighting: the hall lighting and outside lights are operated from the switches at the base of the staircase. To turn the lights upstairs on and off you will need to use the remote control unit found on the oak upright to the left of the dining table. Navigate to units 1, 2, 3 & 4 and press ON/OFF. The remaining 12 channels (5-16) are not in use. The exceptions to this are the colour wheel LED strip lights on the kitchen beams; they use their own remote control."
                                    "\n\nYou will find a little welcome bundle from us, including local milk, tea, bread, coffee, olive oil, marshmallows."
                                    "\n\nPlease feel welcome to help yourselves to anything in our kitchen cupboards. :)"
                                    "\n\nWe also have a Nespresso coffee machine and we try to keep that topped up for our guests. If you do really like your coffee this way, please feel like you can bring along any extra Nespresso compatible capsules you might want for your stay."
                                    "\n\nBeds are made up on arrival. We  leave you a set of towels at the end of your bed, and soap, shampoo, and bubblebath in the bathrooms."
                                    "\n\nWifi. Sure it’s grand, just not suuuuper fast. You can find our wifi code on the back of the router in the kitchen zone."
                                    "\n\nProjector: press the POWER button on at the underside of the projector itself. It is situated just behind the green light. Whilst it is warming up, pull the projection screen down so that it touches the arm of the sofa below it, and hold it in position for several seconds so that it locks in position. Then turn on the amp (beneath the stainless steel worktop in the kitchen); it should already be set up for DVD use, but if necessary use the large dial on the left of the amp frontage to change the function. Volume can be regulated by using the right hand dial. Use the remote control for the DVD player to switch it on/off and to open/close the DVD tray."
                                    "\n\n—Our packing Recommendations—"
                                    "\n\nCosy outdoor blankets to enjoy the fire pit and picnic blankets."
                                    "\n\nOutdoor towels for wild swimming."
                                    "\n\nBlankets and towels for your four legged friends."
                                    "\n\nAppropriate shoes, walking shoes; trainers for activities, sports; riding boots."
                                    "\n\nClothing and footwear suitable for afternoons horse-riding or playing tennis."
                                    "\n\nBinoculars for twitching."
                                    "\n\nYour favourite games, books and DVD’s for the projector this year. We have a big projector that is fun for an evening in, and that can also take a HDMI cable but you wouldn’t then get the benefit of the surround sound experience which we really recommend!"
                                    "\n\n\n——The Chillderness Challenge——"
                                    "\n\n\nPLANTS AND BULBS"
                                    "\n\nAt Chillderness we encourage biodiversity."
                                    "\n\nWe would love you to play a part in our mission."
                                    "\n\nJoin us and bring any plants or bulbs with you, plants that will be meet a good home in Welsh upland pastures."
                                    "\n\nPlant your bulbs and tag us with a photo and the name of your chosen bulb @redkitebarnwales."
                                    "\n\nWe love to be off-grid when we get to go away to Red Kite Estate, so, if you’re like us, don’t worry about tagging us until you get home! And if you’re not into technology at all, we support that and we love you equally. Plant away anyway."
                                    "\n\nCheck out is by 11 tops! Aim for 10:30. :) Enjoy a final cup of tea or coffee and while you’re sipping your brew please feel free to write a sweet review, telling us all about your stay. It’s so useful to us and future guests to hear about the nice details and things people have done, you can tag @Chillderness and @chilldernessretreats in all of your insta stories."
                                    "\n\nWe hope you enjoy your stay. Leave us a little review if you had fun and tag your favourite photos on insta with "
                                    "\n\n#chilldernessmoments"
                                    "\n\nLove from us all at Chillderness HQ"
                                ,
                                style: styles.QuicksandBold(
                                    fontSize: 16
                                ),
                              ),
                            ]
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),),
      ),
    );
  }
}
