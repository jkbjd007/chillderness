import 'package:Chillderness/otherScreen/Late-night-arrivals.dart';
import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class risKiteTreeTent extends StatefulWidget {
  @override
  _risKiteTreeTentState createState() => _risKiteTreeTentState();
}

class _risKiteTreeTentState extends State<risKiteTreeTent> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Red Kite Tree Tents",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 19)
                          )
                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 5),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "Our Guide",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/ridKitRedMenu/ridKitRedMenu.png",),),
            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                            children: <TextSpan>[

                              TextSpan(
                                text: "\nYour adventure begins at 4pm and you’ll soon be soaring in the Tree Tents."
                                    "\n\nWe supply marshmallows, Welsh soap and shampoo, local milk, bread, tea and coffee."
                                    "\n\nFeel free to plant any herbs, bulbs or wild seeds along the river. I was planting basil and mint on our last trip a fortnight ago, see if you can spot it and help yourself. Relax by brewing fresh mint tea by the fire."
                                    "\n\nTry to take away as much rubbish as you can. There is a set of bins just to the right of the turning to the Red Kite Estate as you head back to the main road, and an absolutely brilliant recycling facility in Builth."
                                    "\n\nWhen it’s time to go, it’s time to go. 11am."
                                    "\n\n—Our packing Recommendations—"
                                    "\n\nHead-torch!;"
                                    "\n\nFlip flops for the shower;"
                                    "\n\nYour favourite games and camping recipes (plus the ingredients for these tasty concoctions);"
                                    "\n\nYour favourite hot chocolate powder;"
                                    "\n\nWaterproofs (it’s Wales!)."
                                    "\n\nWe hope you enjoy your stay. Leave us a little review if you had fun and tag your favourite photos on insta with #chilldernessmoments"
                                    "\n\nLove from us all at Chillderness HQ"
                                ,
                                style: styles.QuicksandBold(
                                    fontSize: 16
                                ),
                              ),
                            ]
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),),
      ),
    );
  }
}
