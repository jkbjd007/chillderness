import 'package:Chillderness/otherScreen/Late-night-arrivals.dart';
import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class ridKiteConker extends StatefulWidget {
  @override
  _ridKiteConkerState createState() => _ridKiteConkerState();
}

class _ridKiteConkerState extends State<ridKiteConker> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Red Kite Conker",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 19)
                          )
                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 5),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "Our Guide",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 15,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/conker/conkerMenu.png",),),
            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                            children: <TextSpan>[

                              TextSpan(
                                text: "\nYour adventure begins at 4pm, winding your way up to the Conker."
                                    "\n\nFollow the stepped path to the Conker. You can’t miss it, but if you are arriving after dark please do bring a torch to guide you."
                                    "\n\nMake sure to message us after booking for info on the location of the key."
                                    "\n\nThe Conker runs off mains electricity, so you can charge your phone or kindle whilst sitting in a warm, snug abode. There's also a Bluetooth sound bar for dinner jazz (or goth punk rock)."
                                    "\n\nThe bed is modular, and on arrival it is set out as a table and comfy chairs but at bedtime you will find your linen inside the large octagonal table component and you simply push the constituent parts together to make your inviting bed. The whole is held in place using the velcro strips and the bespoke fitted sheet. Simples!"
                                    "\n\nUnder the tree at the side of the bath there is a stone with a drawing of a tap on top, lift the rock to reveal the actual tap."
                                    "\n\nGeorge Clarke has used it as inspiration for his current build in the new series of Channel 4’s ‘Amazing Spaces’ 2020."
                                    "\n\nIn the morning, wake to a musical outpouring of birdsong; at night, listen for the hooting of owls."
                                    "\n\nThere's a wood-fired pizza oven outside. Bring some dough or chance your arm. We supply some flour and yeast. Make sure to bring the rest!"
                                    "\n\nHomemade pizza is the best."
                                    "\n\nDon’t forget to hashtag #chilldernessmoments with your best sourdough moments."
                                    "\n\nBring some seeds or plugs to plant which are suitable for native upland…we’ve had years of commercial forestry so there aren’t that many varieties there."
                                    "\n#biodiversity, #bringyourown"
                                    "\n\nTell us what you’ve planted and where. As long as it isn’t Japanese knotweed, it’s okay with us!"
                                    "\n\nBe sure to bring warm clothing; preferably waterproof."
                                    "\n\nYou will find tea, coffee, flour, yeast, marshmallows."
                                    "\n\nIn terms of amenities, the Conker has a kettle, hot and cold running water, an electric hob,  basic cleaning utensils, matches, firelighters, etc."
                                    "\n\nThere isn't a fridge so we recommend taking a cool box."
                                    "\n\nWaste and recycling: Obviously we are all working towards zero plastic."
                                    "\n\nBe sure to take as much of your recycling as possible to minimise waste going to landfill."
                                    "\n\nThere are recycling bins at Tesco in Llandrindod Wells."
                                    "\n\nCompost your fruit and veg!"
                                    "\n\nThere's a pit opposite the track in front of the barn for peelings, etc."
                                    "\n\nWhen it’s time to go, it’s time to go. 11am."
                                    "\n\nWe hope you enjoy your stay. Leave us a little review if you had fun and tag your favourite photos on insta with\n #chilldernessmoments"
                                    "\n\nLove from us all at Chillderness HQ"
                                ,
                                style: styles.QuicksandBold(
                                    fontSize: 16
                                ),
                              ),
                            ]
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),),
      ),
    );
  }
}
