import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class lateNightArrivals extends StatefulWidget {
  @override
  _lateNightArrivalsState createState() => _lateNightArrivalsState();
}

class _lateNightArrivalsState extends State<lateNightArrivals> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Late night arrivals",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 19)
                          )
                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: size.convert(context, 4),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "On the RKB estate",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            Container(
              width: size.size362(context, 362),
              height: 170,
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Image.asset("assets/icons/otherIcons/lateNight1.png",
                  width: 120,
                    height: 170,
                  ),
                  Image.asset("assets/icons/otherIcons/lateNight2.png",
                    width: 120,
                    height: 170,
                  ),
                  Image.asset("assets/icons/otherIcons/lateNight3.png",
                    width: 120,
                    height: 170,
                  ),
                ],
              ),),
            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                        style: styles.QuicksandBold(
                            fontSize: 16,
                            color: Colors.black
                        ),
                          text: "If you are passing through Abergavenny en route to the barn, there is a very large Morrisons supermarket. Otherwise, closer to RKB there is a smaller but well-stocked Co-Operative on your way out of Builth Wells, and this  normally closes at 11pm. For a larger local late night supermarket, try Tesco in Llandrindod Wells I think it is 24 hours."
                                  "\n\nClose to home New Bridge on Wye Post Office shop is  a very well stocked Post office come convenience store and newsagent which normally has very long opening hours up to 9pm but has modified hours during Covid. they are on instagram  @newbridge_on_wye_post_office"
                          "\n\nEating out -"
                          "\n\nFun Fact: the Prince Llewellyn Pub at Cilmeri was named after Prince Llewellyn the Last who was ambushed there in 1282. He was the last Ruler of an Independent Wales and ever since then the title has been bestowed on the future King of England. Food is supposed to be ok. Not tried it."
                          "\n\nMichelin stars - for a gastro blow out try Gareth Ward’s Ynyshir but it is a good hour and a half away, Checkers restaurant"
                          "\n\nOr just try our own little bistro at cwm chwefru"
//                            style: TextStyle(
//                              fontSize: 22,
//                            color: Colors.black
//                            )

                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 30,),
        //
          ],
        ),),
      ),
    );
  }
}
