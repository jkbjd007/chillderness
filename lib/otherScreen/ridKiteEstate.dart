import 'package:Chillderness/otherScreen/Late-night-arrivals.dart';
import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class ridKiteEstate extends StatefulWidget {
  @override
  _ridKiteEstateState createState() => _ridKiteEstateState();
}

class _ridKiteEstateState extends State<ridKiteEstate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                    color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                    text: TextSpan(
                      text: "Red Kite Estate",
                      style: styles.PlayfairDisplayBoldItalic(
                        fontSize: size.convert(context, 19)
                      )
                    ),
            ),
                  ),
                ],
              ),),
            SizedBox(height: 5),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "Enjoy a day out at home in our 80 acre playground",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 15,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/otherIcons/ridKiteEstate.png",),),
            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                              text: "A Simpler Life",
                              style: styles.PlayfairDisplayBoldItalic(
                                fontSize: 14
                              ),
                            ),

                            TextSpan(
                              text: "\nOut the door to the moors. Zero minutes.\n"
                                "Take a walk to the end of the drive, through the\n"
                                  "gate, turn right and strike up and out over the\n"
                                  "desert of Wales."
                              "\n\nAn hour’s circuit would take you in a semicircle \nround the back of the barn and above the conker \nwhere you can see the Red Kites Soar. You’ll \ndefinitely see a few."
                                  "\n\nTake your study all the way and visit the Red Kite \nFeeding Station at Gigrin Farm for close\nencounters. Huge legs of meat are thrown out and\nwatch those birds descend."
                                "\n\nIn theory, you could go all the way over the\nNational Trusth Moorland to Aberystwyth passing\nthe historic Strata Florida Arch, but having done it\nonce on a quad bike getting stuck in several bogs it\nwas funny but it may be safer to drive.",
                              style: styles.QuicksandBold(
                                  fontSize: 14
                              ),
                            ),

                          ]
                        ),
                      ),
                      ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: "Itch to twitch",
                                style: styles.PlayfairDisplayBoldItalic(
                                    fontSize: 14
                                ),
                              ),

                              TextSpan(
                                text: "\nTwitching - early morning you will hear a deafening\nDawn Chorus or walk out late at night and hear the\nhooting of owls. Bring binoculars if you can. Let us\nknow on the app which birds you’ve spotted and\nwhere.\n"
                                  "Tag @chilldernessretreats with the bird name and\nwe’ll try and add you to the nature montage.",
                                style: styles.QuicksandBold(
                                    fontSize: 14
                                ),
                              ),
                            ]
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: "Contribute to biodiversity",
                                style: styles.PlayfairDisplayBoldItalic(
                                    fontSize: 14
                                ),
                              ),

                              TextSpan(
                                text: "\nBring some seeds or plugs to plant which are\nsuitable for native upland. Tell us what you’ve\nplanted and where. As long as it isn’t Japanese\nknotweed, it’s cool! #biodiversity, #bringyourown",
                                style: styles.QuicksandBold(
                                    fontSize: 14
                                ),
                              ),
                            ]
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(height: 30,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: "Build a fire in the pit",
                                style: styles.PlayfairDisplayBoldItalic(
                                    fontSize: 14
                                ),
                              ),

                              TextSpan(
                                text: "\nI’m stating the obvious but what tastes better than toasted marshmallows sandwiched between chocolate biscuits. The classic SMORE."
                                  "I find that the smaller marshmallows are better as the large ones go black before they do mushy."
                              "Cook your dinner on the fire. Pack your potatoes round the embers. Then toast your smores."
                              "Hashtag @chilldernessretreats with your gooiest",
                                style: styles.QuicksandBold(
                                    fontSize: 14
                                ),
                              ),
                            ]
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(height: 30,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RichText(
                        text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: "At the Conker",
                                style: styles.PlayfairDisplayBoldItalic(
                                    fontSize: 14
                                ),
                              ),

                              TextSpan(
                                text: "\nWe have a pizza oven outdoor. Bring some dough or chance your arm. We supply some flour and yeast. The rest is for you"
                                "\n\nHomemade pizza is the best."
                                "\n\nDon’t forget to hashtag @Chilldernessretreats with your best sourdough moments…"
                                "\n\nTALGARTH FLOUR MILL SUGGESTION"
                                "\n\nRestored water mill, tea rooms etc. Visited recently by Charles and Camilla for flour and sourdough etc at 2.70 per 800g."
                                "\n\nWILD SWIMMING"
                                "\nIn my day this was just called swimming outdoors but its fantastically invigorating. Bring some jelly shoes if you remember…good footwear makes the difference between a great holiday and a good one."
                                "\n\nJelly shoes if you remember then for climbing over spiky rocks."
                                "\n\nPendoll which is quite close by is lovely, flat rocks and a slow flowing river spreading out into a lake"
                                "\n\nElan valley junction pool with wobbly bridge is fun for the kids and fed with water from the dam [damn cold!]. "
                                "\n\nIrfan rocks [Wolf’s leap] is supposed to be ace. "
                                "\n\nOr you can just go to Ynys las to the sea if it’s a lovely day and gorge on retro Mr Whippys. Make sure you have the hazelnut sprinkles and chocolate sauce"
                                "\n\n\nOTHER IDEAS: RE-ENGAGE"
                                "\n\nSpot hares"
                                  "\n\nCount sheep"
                              "\n\nPaint watercolours",

                                style: styles.QuicksandBold(
                                    fontSize: 14
                                ),
                              ),
                            ]
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),),
      ),
    );
  }
}
