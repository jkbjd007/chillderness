
import 'package:Chillderness/doNearBy.dart';
import 'package:Chillderness/nearWalksMenu.dart';
import 'package:Chillderness/otherScreen/Late-night-arrivals.dart';
import 'package:Chillderness/otherScreen/ridKiteBarn.dart';
import 'package:Chillderness/otherScreen/ridKiteEstate.dart';
import 'package:Chillderness/res/color.dart';
import 'package:Chillderness/res/urlLauncher.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';

import 'beginingNavigation.dart';
class ridKitBarnMenu extends StatefulWidget {
  @override
  _ridKitBarnMenuState createState() => _ridKitBarnMenuState();
}

class _ridKitBarnMenuState extends State<ridKitBarnMenu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                    color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                  text: TextSpan(
                    text: "Welcome to the Red Kite Barn,",
                    style: styles.PlayfairDisplayBoldItalic(
                      fontSize: size.convert(context, 19)
                    )
                  ),
            ),
                ],
              ),),
            SizedBox(height:  5,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "It's great to have you staying with us",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 16),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/ridKitBarn/ridKitBarnMenu.png",
                width: size.size362(context, 362),),),
            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/mapIocn.png",),),
            SizedBox(height: 15,),
            GestureDetector(
              onTap: (){
//                UrlLaunch.OpenGoogleMap("https://goo.gl/maps/YF4RQzW2XvJLavAx7");
                  Navigator.push(context, PageTransition(child: beginingNavigation(),
                  type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                  color: buttonColor,
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Navigate to the Red Kite Barn",
                        style: styles.PlayfairDisplayBlack(
                          fontSize: size.convert(context, 12,),
                          color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, PageTransition(child: ridKiteBarn(),
                    type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Details and help for your stay",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                print("late night arrivals");

                Navigator.push(context, PageTransition(child: lateNightArrivals(),
                    type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Late night arrivals",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, PageTransition(child: ridKiteEstate(),
                    type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "On site experiences",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){


                Navigator.push(context, PageTransition(child: nearWalksMenu(),
                    type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "View nearby walks",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, PageTransition(child: doNearBy(),
                    type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "View fun activities nearby",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                UrlLaunch.BrowseLocalOffers();
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Browse Local Offers",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                UrlLaunch.VisittheInstagram();
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Visit the Instagram",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                UrlLaunch.Reviewus();
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Review us",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/colorLogo.png",),),
            SizedBox(height: 10,),
          ],
        ),),
      ),
    );
  }
}
