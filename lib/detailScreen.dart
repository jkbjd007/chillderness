import 'package:Chillderness/otherScreen/Late-night-arrivals.dart';
import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class detailScreen extends StatefulWidget {
  String titleIcon;
  String title;
  String subTitle;
  String detail;
  String icon;
  String icon2;
  String titleIcon1;
  String titleIcon2;
  String headerIcon1;
  String headerIcon2;
  String subIcon1;
  String subIcon2;
  detailScreen({this.title,this.detail,this.icon,this.subTitle,this.titleIcon,this.icon2,this.headerIcon1,this.headerIcon2,this.subIcon1,this.subIcon2,this.titleIcon1,this.titleIcon2});
  @override
  _detailScreenState createState() => _detailScreenState();
}

class _detailScreenState extends State<detailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  (widget.headerIcon1 == null || widget.headerIcon1 == "") ? Container() :Container(child: Row(
                    children: <Widget>[
                      (widget.headerIcon1 == null || widget.headerIcon1 == "") ? Container() : Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Image.asset(widget.headerIcon1,),),

                      (widget.headerIcon2 == null || widget.headerIcon2 == "") ? Container() : Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Image.asset(widget.headerIcon2,),),
                    ],
                  ),),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: widget.title,
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 19)
                          )
                      ),
                    ),
                  ),
                  (widget.titleIcon1 == null || widget.titleIcon1 == "") ? Container() :Container(child: Row(
                    children: <Widget>[
                      (widget.titleIcon1 == null || widget.titleIcon1 == "") ? Container() : Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Image.asset(widget.titleIcon1,),),

                      (widget.titleIcon2 == null || widget.titleIcon2 == "") ? Container() : Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Image.asset(widget.titleIcon2,),),
                    ],
                  ),),
                ],
              ),),
            SizedBox(height: size.convert(context, 4),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: widget.subTitle,
                          style: styles.PlayfairDisplayBlack(
                              fontSize: size.convert(context, 12)
                          )
                      ),
                    ),
                  ),
                  (widget.subIcon1 == null || widget.subIcon1 == "") ? Container() :Container(child: Row(
                    children: <Widget>[
                      (widget.subIcon1 == null || widget.subIcon1 == "") ? Container() : Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Image.asset(widget.subIcon1,),),

                      (widget.subIcon2 == null || widget.subIcon2 == "") ? Container() : Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Image.asset(widget.subIcon2,),),
                    ],
                  ),),
                ],
              ),),
            SizedBox(height: size.convert(context, 10),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset(widget.titleIcon),),
            SizedBox(height: 20,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(

                      child: RichText(
                        text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: widget.detail,
                                style: styles.QuicksandBold(
                                    fontSize: 16
                                ),
                              ),

                            ]
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20,),
            (widget.icon2 == null || widget.icon2 == "") ? Container() : Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset(widget.icon2),),

            (widget.icon == null || widget.icon == "") ? Container() :SizedBox(height: 10,),
            (widget.icon == null || widget.icon == "") ? Container() : Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset(widget.icon),),

            (widget.icon == null || widget.icon == "") ? Container() :SizedBox(height: 10,),

          ],
        ),),
      ),
    );
  }
}
