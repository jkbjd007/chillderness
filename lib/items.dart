import 'package:flutter/cupertino.dart';

List ViewNearWalks = [
  {
    "id": "1",
    "title": "Walk the moors",
    "subTitle": "And go to the pub! Win-win",
    "titleIcon": "assets/icons/nearbyWalks/walkMoor.png",
    "walkType": "Walks the Moors",
    "walkLimit": "3 hours",
    "detail": "A pleasant walk, with a real purpose, is from Red Kite Estate to the local pub, the Red Lion, Llanafan Fawr, reputedly the oldest pub in Powys. At the moment (summer 2020), the pub is in the process of renovation by its new owner, expected to reopen shortly, at the beginning of August, with a good selection of ales and good food."
        "\n\nTo walk over the moorland, head left out of the Red Kite Estate wooden gate and walk down the lane keeping the conifer plantation on your right. At the end of the woodland, before the national trust gate [around the entrance to Cwm Clyd Uchaf] turn right up the pathway to the brow of the hill and down the other side. Some huffing and puffing time later you will come to a tarmac roadway turn left and walk to the end of the road."
        "\n\nIt is a very beautiful road, gentle Welsh valleys complete with river and sheep. When you join the main road, turn to your right and go downhill and uphill until you come to the pub on your right. The local church is opposite with a certified 2,000 year old yew tree in the graveyard. The previous landlords’ jokes are even older! To book a table call on 01597 860 204.",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "2",
    "title": "Gone Fishing",
    "subTitle": "A hop, a skip, and a picnic!",
    "titleIcon": "assets/icons/nearbyWalks/goneFishing.png",
    "walkType": "Gone Fishing",
    "walkLimit": "Half day",
    "detail": "Another of our favourite walks  with a purpose, is from us to Cwm Chefru Estate, a little cluster of cottages with an absolutely gorgeous lake. The lake is filled with brown trout, and you can fish here [catch and release] for a small fee."
        "\n\nThe route begins in the same way as Walk the Moors, but when you hit the single track road after the huffing and puffing bit [which is essentially parallel to ours] but very much less savage in its landscape, you turn right rather than left. The road is long and low with idyllic scenery of hills on one side and the river running through on the other. It really is a rural idyll. "
        "\n\nFor a lakeside lunch in the grounds, fabulous Kate Lowe puts together delightful picnics and can have a bottle of chilled wine waiting! "
        "\n\nShe runs a tiny bistro, just look at @the_bistro_cwm_chwefru :) She likes insta messages, or reach out to her at katejlowe@aol.com or 07740103753. "
        "\n\nKate can also drop takeaway picnics and dinners to Red Kite Estate."
        "\n\nTENNIS OR FISHING"
        "\n\nAmy at Cwm Chefru can set you up with fishing for a modest fee.20 for the fishing including a rod for an adult and maybe just a contribution for a child. "
        "\n\nNo Wimbledon this year so ‘don your whites’, and have a game. Cwm Chefru has tennis court and rackets - or bring your own.  What a setting for a game."
        "\n\nFor activities contact Amy at "
        "\n\n01597860447 or at"
        "\n\nreception@cwmchwefrucottages.co.uk"
        "\n\nThe only downside to this afternoon out is no dogs. :(",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "3",
    "title": "Elan Valley Trail",
    "subTitle": "Explore the valley",
    "titleIcon": "assets/icons/nearbyWalks/valleytrail.png",
    "walkType": "Elan Valley Trail",
    "walkLimit": "Day trip",
    "detail": "Walking Distance: 9 miles\n"
        "\n\nThe Elan Valley Trail is a linear path following the line of the old Birmingham Corporation Railway. The trail starts in Cwmdauddwr, just west of Rhayader and finishes 13 km (9 miles) further up the Elan Valley at Craig Goch Dam."
        "\n\nThe trail is open for use by walkers, cyclists and horse riders. As on bridleways, cyclists and horse riders are required to give way to walkers and the less-able.",
    "icon": "assets/icons/nearbyWalks/valleyMap.png"
  },
  {
    "id": "4",
    "title": "Pen Y Fan",
    "subTitle": "Strange name, awesome place",
    "titleIcon": "assets/icons/nearbyWalks/panyFan.png",
    "walkType": "Pen Y Fan",
    "walkLimit": "Half/Day trip",
    "detail": "Oh, what views!"
        "\n\nPen y fan is perhaps the most impressive of all the mountains in the Brecon Beacons."
        "\n\nWhile the car park will probably be jammers during weekend visits, once you have engaged on the walk to the top, you will notice the unbounded distances between you and anyone else, how first world problems fade away as you are submerged in one of the most beautiful hillsides we have ever seen."
        "\n\nOnce at the top of the tabletop hill you can see far and wide, with a feeling that you have conquered all of Wales. The view of the surrounding lakes and reservoirs is forever memorable. ",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "5",
    "title": "The 4 Waterfall Walk",
    "subTitle": "And go to the pub! Win-win",
    "titleIcon": "assets/icons/nearbyWalks/waterFallWalk.png",
    "walkType": "The 4 Waterfall Walk",
    "walkLimit": "Day trip",
    "detail": "This quadruple waterfall walk is a ton of fun! "
        "\n\nIt can be easy or hard based on the routes you take and great for the whole family."
        "\n\nAgain, we’re talking the Brecons, so a little way of a drive but you could end a great day out after the Pen y fan walk with an afternoon at these waterfalls on the return journey back to your Red Kite home. We did it even with my ten year old sister, but equally you could just make it the object of the day. "
        "\n\nEach waterfall is special but my favourite is the third waterfall. You can park near it. Walking behind it is an awesome and surreal experience (bring a poncho!) and very different to looking at it from the outside - you can experiment with sound and touch. We can’t rate it highly enough. Sure this is fun for all the family too!"
        "\n\nThe paths around the river can get a bit mushy so we recommend bringing some proper walking boots/shoes for the day out",
    "icon": "",
    "icon2": "",
  }
];

List doNearByList = [
  {
    "id": "1",
    "title": "Canoe in the Wye Valley",
    "subTitle": "Explore the valley by canoe",
    "titleIcon": "assets/icons/doNearBy/WyeValley.png",
    "walkType": "Canoe in the Wye Valley",
    "walkLimit": "Day trip",
    "detail": "Wye Valley Canoe Centre for beds, bikes and canoes and  a rather superior sort of bacon sandwich at the at the AA Gill recommended River Cafe at Glasebury on Wye - “well worth the detour, if not from London from anywhere else in Wales” - supercilious git, but read the review anyway (Times, June 19, 2011). I think the management may have changed but we’d love to know what you now think of the food. You can stay here too if you fancy moving away from us..01497847007"
        "\n\nPaddle 5.5 miles downstream to the town of Hay-on-Wye for a half day trip or full day to Whitney from £20 pp. They will bring you back for a slap up tea at the River Caff or even dinner on certain days of the week."
        "\n\nFun for all ages, little’uns may need a guide."
        "\n\nYou’d also be in the right area for the Wye Valley Walk."
        "\nKeen cyclists could even bike the 15.5 miles to Glasebury from Builth on the sustains track. A great route with some short steep sections…."
        "\nAn alternative for guides and canoes is www.Celticcanoes.co.uk",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "2",
    "title": "Cycling in the Elan Valley",
    "subTitle": "Explore the valley by bike",
    "titleIcon": "assets/icons/doNearBy/elanValley.png",
    "walkType": "Cycling in the Elan Valley",
    "walkLimit": "Half day",
    "detail": "Cycling is a great way to see a lot more of Elan. You can pick up bikes from the  Elan Valley Visitor Centre in town, have a preview at www.clivepowellbikes.com. Or Elan Cyclery: 01597811343"
        "\n\nThere are also off-trail motor trails and I think Clive Powell knows about them but I don’t"
        "\n\nThe Elan Valley Trail is a linear path following the line of the old Birmingham Corporation Railway. The trail starts in Cwmdauddwr, just west of Rhayader and finishes 13 km (9 miles) further up the Elan Valley at Craig Goch Dam."
        "\n\nScroll for prices and map. 😊",
    "icon": "assets/icons/nearbyWalks/valleyMap.png",
    "icon2": "assets/icons/doNearBy/priceList.png",
  },
  {
    "id": "3",
    "title": " Feeding and Flying Raptors",
    "subTitle": "Nothing to do with a bridge, sorry",
    "titleIcon": "assets/icons/doNearBy/flyingRaptors.png",
    "walkType": " Feeding and Flying Raptors",
    "walkLimit": "Day trip",
    "detail": "Engage with the eponymous heroes of our tale. Red Kites are spectacular. Head over to Gigrin Farm and nearby Rhayader to behold the royal red kite rulers of the Powys region.  Catch a pint at the cute triangle pub."
        "\n\nRehabilitated from the edge of extinction, now you might see between 300-500 of these wonderful birds of prey up close as they devour free lunches kindly provided to them by the caring team at Rhyader Feeding Station. Communal lunch for the birds starts at 2pm and will last a good hour."
        "\n\nArrive anytime from 1pm and have a walk and coffee in the grounds."
        "\n\nWell-behaved dogs, and photographers, are welcome too."
        "\n\nBook very reasonably priced tickets (£4-£7.50) in advance at: https://www.gigrin.co.uk/visitor-information/prices/."
        "\n\nMajestic."
        "\n\nIf you love this experience, ring Eddie and Cara at E and C Falconry for an opportunity to fly Ruby and Kiff and Pebbles. 07917826666. They can even bring them to Red Kite Estate. "
    ,
    "icon": "",
    "icon2": "",
  },
  {
    "id": "4",
    "title": "Bridge over the river Thai",
    "subTitle": "Nothing to do with a bridge, sorry",
    "titleIcon": "assets/icons/doNearBy/riverThai.png",
    "walkType": "Bridge over the river Thai",
    "walkLimit": "2 hours",
    "detail": "A great place to stop off for a hearty meal. You have got to try this one."
        "\n\nJust the Newbridge side of the river at Builth Wells, the Llanelwedd Arms Hotel is an old-fashioned Welsh pub with a surprisingly good and authentic range of Thai dishes, together with a decent offering of beers and cider on tap."
        "\n\nFlooded during the early part of 2020 it expects to re-open at the beginning of August 2020. Book a table on 01982 553 282. ",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "5",
    "title": "Newbridge on Wye Carnival",
    "subTitle": "10-odd dalmations",
    "titleIcon": "assets/icons/doNearBy/wyeCarnival.png",
    "walkType": "Newbridge on Wye Carnival",
    "walkLimit": "Day trip",
    "detail": "Every summer, the village puts on a carnival with a crazy flotilla, tractors, trailers, classic cars and marching bands galore. "
        "\n\nThe carnival is well worth attending. The gala is a great afternoon too. "
        "\n\nThe 2020 Carnival will not take place this year… Here’s to 2021 and beyond! ",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "6",
    "title": "Pony trekking",
    "subTitle": "Mummy, I want a pony.",
    "titleIcon": "assets/icons/doNearBy/Ponytrekking.png",
    "walkType": "Pony trekking",
    "walkLimit": "3/4 Day trip",
    "detail": "If you fancy saddling up and riding out in the beautiful Welsh hills and through the River Wye, try the local Lion Royal Pony Trekking just outside Rhayader.  They have been going for 55 years."
        "\n\nIt’s pleasant, if a bit sedate, but good for a family ride if some are less experienced and not too dear (35 or so for 2 hours). "
        "\nCall 01597 810 202 to book."
        "\n\nSadly they are closed this year due to Covid but you can take a slightly longer drive to Underhill to friends Jess and Cath who can take you out in the forestry or give you a cross country lesson. Say you are friends of Inigo if you go."
        "\n\nWear jeans if you don’t have a pair of jodhpurs in your holiday wardrobe, and they will sort you out with a pair of boots for you if you don’t want to splash out on a new pair in their adjacent tack room. A little more expensive but they make your life easy here and you do get a few long canters in and see some good scenery."
        "\n\ninfo@underhillridingstables.co.uk"
        "\n01597851890",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "7",
    "title": "Elan Valley Reservoirs",
    "subTitle": "Wow, just wow",
    "titleIcon": "assets/icons/doNearBy/ValleyReservoirs.png",
    "walkType": "Elan Valley Reservoirs",
    "walkLimit": "Day trip",
    "detail": "A vintage collection of huge reservoirs and breathtaking dams."
        "\n\nThis particular series of insanely monolithic and impressive dams was created by the Victorians to gravity feed water to the industrial hub of England in Birmingham, using an unimaginable amounts of stone in impressive scenery. Don’t fall in. Just nearby, wild swimming in the Elan Junction pool is to enjoy its freezing waters"
        "\n\nAberystwyth to Rhayader or vice versa"
        "\n\nThere is a lovely scenic road that skirts the reservoirs and brings you back into Rhayader. Or if you decide to go to the beach at Borth you can do the whole nine yards and take the road over the mountains [not good for the carsick].  Here is the route in reverse…"
        "\n\nThe Cambrian Mountains form the spine of Wales, and the prettiest route across them is to head out of Aberystwyth on the A4120 to Devil’s Bridge, follow the B4574 to Cwmystwyth, then pick up the tremendous mountain road that soars through the mountains, past the northernmost of the Elan Valley reservoirs, before eventually dropping down into Rhayader. "
        "\n\nOn the way: Aberystwyth is a classy resort and university town with plenty for visitors to do – including a steam train journey up to Devil’s Bridge (actually three bridges, built on top of each other in a dizzying gorge). The estate is now a tranquil paradise for walkers and wildlife.",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "8",
    "title": "Dyfi Nature Reserve",
    "subTitle":
        "Run down the beach (or just eat retro diesel filled ice cream)",
    "titleIcon": "assets/icons/doNearBy/NatureReserve.png",
    "walkType": "Dyfi Nature Reserve",
    "walkLimit": "3 hours",
    "detail": "Borth, just north of Aberystwyth, on Wales’ western shoreline has a really long flat duney beach fringed with Maram grasses. Big skies, big grey seas. Blow the cobwebs out. "
        "\n\nTake your kite surf if you have one, if not, try a lesson: surf or canoe at Aber adventures, 07976061514."
        "\n\nKeep heading north along the coastline through Ynys Las itself until you come to the Dyfi National Nature Reserve."
        "\n\nGreat sand dunes to explore, with beach and water on each side. You can park up on the beach itself (£2 charge) and watch kite surfers, paddle-boarders, doggo’s and doggy-paddling children, and glamorous horses alongside the retro ice cream van.Delicious nut speckled Mr Whippy 99’s. It even has a tasty little vegan number… ",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "9",
    "title": "The Willow Globe",
    "subTitle": "Uniquely natural setting for outdoor theatre",
    "titleIcon": "assets/icons/doNearBy/WillowGlobe.png",
    "walkType": "The Willow Globe",
    "walkLimit": "Half day",
    "detail": "Tucked away off the main road between Newbridge-on-Wye and Rhyader, Y Glob Byn, is a little gem of a cultural venue. LD1 6NN"
        "\n\nSo-called because the amphitheatre is entirely encircled by tall willow trees,"
        "\n\nMany a band of travelling players and minstrels treads its boards; last summer the Willow Globe hosted"
        "\n‘The Three Inch Fools’ slick production of Shakespeare’s comedy Much Ado About Nothing. This was a summer delight. An enthusiastic posse of volunteers serves up light meals and drinks before the plays and during the interval."
        "\n\nThe whole experience is charmingly different and not to be missed...but, oh, pack a kagool!"
        "\n\nThere are Have a go Shakespeare sessions and lectures this season. Follow online.",
    "icon": "",
    "icon2": "",
  },
  {
    "id": "10",
    "title": "Pottery!",
    "subTitle": "Such fun",
    "titleIcon": "assets/icons/doNearBy/Pottery.png",
    "walkType": "Pottery!",
    "walkLimit": "4 hour",
    "detail": "Spending your afternoon in this compact yet charming pottery shed-studio is the perfect activity for a rainy day just down the road in New Bridge on Wye."
        "\n\nThis is fun for all ages and levels of experience."
        "\n\nWhilst there, you can either hand build in clay or spin on the pottery wheel with guidance available from the wonderful, helpful Alex Allpress. He’ll even fire your earthen masterpieces in his kiln for a little fee, available for pickup soon after."
        "\n\nTop tips: Remember to bring clothing that you don’t mind getting mucky, and a coat during colder season. To book a session, contact Alex on 01982552854 or 07443875016. Quite a little studio so limited spaces available.",
    "icon": "",
    "icon2": "",
  },
];

List chaletList = [
  {
    "id": "1",
    "title": "Col de la Forclaz",
    "subTitle": "We will not forclaz on your home",
    "titleIcon": "assets/icons/chalet/ColdelaForclaz.png",
    "walkType": "Col de la Forclaz",
    "walkLimit": "Day trip",
    "detail": "\nThe flattest and quickest way, to Les Houches would be heading towards Col de la Forclaz by starting to walk up from the chalet towards Les Plancerts (ski lift). The hardier will head for Col de la Forclaz which should be signposted according to the lovely Raf and Jessie. We have yet to do this on foot!"
        "\n\nThis view point is set between Tête Noire and Le Prarion allowing us to approach two different peaks if we choose to,  or just to head towards the village of Les Houches."
        "\n\nOn this walk (same as the “Les Houches” one) you cross plenty of creeks, and see tumbling"
        "\nwaterfalls onto the path."
        "\n\nThe walk is reasonably flat, deeply immersed in the Forest."
        "\n\nOnce arriving at Col de la Forclaz there is a possibility of a loop by following the sign back to Montfort (next to chalet carpark)."
        "\n\nTake your packed lunch, sunscreen, and a bottle of rose - there will be streams to drink from. Check if La Tanière or La Cha is open.",
  },
  {
    "id": "2",
    "title": "Les Houches",
    "subTitle": "Skis, no bees and a general lack of trees",
    "titleIcon": "assets/icons/chalet/LeHouches.png",
    "walkType": "Les Houches",
    "walkLimit": "Day trip",
    "detail": "\nThere are many ways to get to Les Houches without a need of a vehicle. It’s quite a humble destination compared with neighbours glamorous Megeve, or sporty Chamonix except climbing its slopes to do mountain biking , hiking or skiing gives you one of the best 360 views in the world from the top of dents du midi and the"
        "\n\nThe flattest and quickest way, would be heading towards Col de la Forclaz by starting to walk up from the chalet towards Les Plancerts (ski lift)."
        "\n\n After crossing the slope, there will be a sign pointing Les Houches way that will take you on a flat route to the left of Le Prarion.",
  },
  {
    "id": "3",
    "title": "Tete Noire",
    "subTitle": "Green",
    "titleIcon": "assets/icons/chalet/TeteNoire.png",
    "walkType": "Tete Noire",
    "walkLimit": "Day trip",
    "detail": "\nThe greenest summit around the area. Stirke out for a bit of forest bathing or shrinyinyoku"
        "\n\nYou wouldn’t necessarily go for here for 360 views but you might go here for a nice walk and a little bit of remote peace and quiet."
        "\n\nTete Noire is characterized by a small plateau forming a silent tree shrine with a few open spaces for a deserved picnic."
        "\n\nEasily approached from Col de la Forclaz, by following a well-marked and easy path.",
  },
  {
    "id": "4",
    "title": "Bionnassay",
    "subTitle": "Tall Mountain",
    "titleIcon": "assets/icons/chalet/Bionnassay.png",
    "walkType": "Bionnassay",
    "walkLimit": "Day trip",
    "detail": "\nIf we picture a pyramid"
        "\nrepresenting Le Prarion"
        "\nmountain, the highest vertex being the summit, then one vertex"
        "\nwould be Les Houches, the other Chalet F’net (Saint Gervais)"
        "\nand the last would belong to Bionnassay."
        "\n\nBionnassay offers fantastic walks, taking you right to the Glacier bowl where its immensity is bound to dazzle you."
        "\n\nTo get to Bionnassay, the quickest route is to wrap the mountain from the right side, passing La Tanière restaurant and continuing down to our right until next sign where we"
        "\nwill make a left and start walking up again until we cross Mont Blanc tramway track."
        "\n\nFrom this point, our life becomes a lot easier by simply following the train track via a bottom path. You will reach a crossroads sign where you will be able to choose whether to head down to Bionnassay carpark or proceed up to your left, by crossing the train track again towards Le Prarion hotel."
        "\n\nThere are scenic loops available in this area very accessible from Bionnassay carpark. It’s quite well signposted or you can take the tram.",
  },
  {
    "id": "5",
    "title": "Saint-Gervais",
    "subTitle": "Another town walk!",
    "titleIcon": "assets/icons/chalet/SaintGervais.png",
    "walkType": "Saint-Gervais",
    "walkLimit": "Day trip",
    "detail": "\nYou’ve got to get up before you get down some times. To go to St Gervais  you hike up (via La Tanière for morning coffee if she is open…)  and then"
        "\n\nThis walk has a steady decent at the start, which gradually turns into a steeper decline to the town center. Or you can just walk down the road if you want."
        "\n\nThe walk is a dirt road easy to follow and is well maintained, you also cross the train track halfway down."
        "\n\nThe route passes a few chalets along the way but is a very quiet route, with the odd walker and their dog. ",
  },
  {
    "id": "6",
    "title": "Montfort",
    "subTitle": "A flat mountain walk!",
    "titleIcon": "assets/icons/chalet/Montfort.png",
    "walkType": "Montfort",
    "walkLimit": "Day trip",
    "detail": "\nIf you take a right at the bottom of the track to the chalet and walk across the car park, you’ll enjoy a fairly flat walk to the little village of Montfort."
        "\n\nThis is undoubtably the walk where you’ll find most runners as it is the longest flat route in the area."
        "\n\nIt is great for an active rest day."
        "\n\nQuench your thirst in the Montfort fountain when you arrive into the village. This one freezes over in Winter.",
  },
  {
    "id": "7",
    "title": "St Gervais II",
    "subTitle": "A really nice off the beaten track walk to town",
    "titleIcon": "assets/icons/chalet/StGervais.png",
    "walkType": "St Gervais II",
    "walkLimit": "Day trip",
    "detail": "\n(via Cheminées des Fées)"
        "\n\nThis trail is one of the nicest ‘off the beaten track’ routes taking you down to Saint Gervais."
        "\n\nPassing through Montfort, you make a left down towards Saint Gervais town."
        "\n\nThe route has a mix of steady, steep, and flat paths."
        "\n\nYou cross a few streams along the way and if it’s a sunny day you are well sheltered by the trees."
        "\n\nFor daytime, rather than evening, descent…",
  },
  {
    "id": "8",
    "title": "La Cha + La Tanière",
    "subTitle": "We will not forclaz on your home",
    "titleIcon": "assets/icons/chalet/LaChaLaTanière.png",
    "walkType": "La Cha + La Tanière",
    "walkLimit": "Dindins",
    "detail": "\nWithin skiing or walking distance of Chalet F’Net are two fun restaurants that we really recommend, both within the Les Houches domaine."
        "\n\nLa Cha is just below the top of the mildly mischievous Plancerts drag lift: you get an adrenalin kick if you don’t get chucked off by this boisterous 1970s wonder. For those accustomed to the giant restaurants of the Three Valleys mega resorts, La Cha is a cutsey little place for lunch, and the food is good, and generous: a juicy steak, a mountain of chips, a glass of pretty decent Rhone red. The food isn’t fancy but the views are incredible if you sit out on the terrace drinking rose and eating hot dogs. Vincent and Céline are friendly hosts. Outdoor BBQ and bar in the ski season; umbrellas and assiettes of cured meats in summer. You can walk up from the chalet when the snow has melted; you’ll be in serious need of one of their refreshing beverages when you get there."
        "\n\nLa Tanière is run by the characterful Louisa. You can walk up from the chalet or ski down from above the restaurant, although the Plan du Cret run can be gritty and isn’t really suitable for beginners. Really good food, especially the Argentine dishes and homemade tarts. A good place for a glass of rosé at the end of the day. Sit inside by the fire or (better) out on the terrace, with beautiful views of the Chaîne des Aravis. This place feels quite cut off – probably because it is. In summer, the service is much more patchy: you will need to book ahead and it is only open at weekends really, or when Louise fancies it. The fun is when you can sledge home half cut to the chalet. Note: cash only!",
  },
];

List chaletDoNearBy = [
  {
    "id": "1",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "La Cha + La Tanière",
    "subTitle": "Two skiable or walkable restaurants from F’Net",
    "titleIcon": "assets/icons/chalet/LaChaLaTanière.png",
    "walkType": "La Cha + La Tanière",
    "walkLimit": "Dindins",
    "detail": "\nWithin skiing or walking distance of Chalet F’Net are two fun restaurants that we really recommend, both within the Les Houches domaine."
        "\n\nLa Cha is just below the top of the mildly mischievous Plancerts drag lift: you get an adrenalin kick if you don’t get chucked off by this boisterous 1970s wonder. For those accustomed to the giant restaurants of the Three Valleys mega resorts, La Cha is a cutsey little place for lunch, and the food is great, and generous: a juicy steak, a mountain of chips, a glass of pretty decent Rhone red. The food isn’t fancy but the views are incredible if you sit out on the terrace. Vincent and Céline are friendly hosts. Outdoor BBQ and bar in the ski season; umbrellas and assiettes of cured meats in summer. You can walk up from the chalet, when snow has melted, but you’ll be in serious need of one of their refreshing beverages when you get there."
        "\n\nLa Tanière is run by the characterful Louisa. You can walk up from the chalet or ski down from above the restaurant, although the Plan du Cret run can be gritty and isn’t really suitable for beginners. Really good food, especially the Argentine dishes and homemade tarts. A good place for a glass of rosé at the end of the day. Sit inside by the fire or (better) out on the terrace, with beautiful views of the Chaîne des Aravis. This place feels quite cut off – probably because it is. In summer, the service is much more patchy: you will need to book ahead and it is only open at weekends really, or when Louise fancies it. The fun is when you can sledge home half cut to the chalet. Note: cash only!",
  },
  {
    "id": "2",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Mont Blanc Gin Distillery",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/MontBlancGinDistillery.png",
    "walkType": "Mont Blanc Gin Distillery",
    "walkLimit": "Half day",
    "detail": "\nWhere do you find a Scotsman in a kilt in the Alps?"
        "\n\nThe entrepreneurial and multi-talented James Abbott has set up the Mont Blanc gin distillery at the top of the mountain above our chalet."
        "\n\nThe distillery is next door to the Tanière restaurant. This distinctive gin is flavoured with all the herbs of the Prarion."
        "\n\nYou can sample Mr Abbott’s wares at several local bars and restaurants."
        "\n\nIt might not be whisky, and this isn’t Scotland, but we definitely recommend a wee dram. Say we sent you {after you’ve sampled} he isn’t so fond of us but we don’t hold it against him."
        "\nhttps://www.distilleriesaintgervais.com",
  },
  {
    "id": "3",
    "headerIcon1": "assets/icons/chalet/nearBy/franceflag.png",
    "headerIcon2": "assets/icons/chalet/nearBy/unitedkingdomflag.png",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Saint-Gervais Mont-Blanc Thermal Spa",
    "subTitle": "A charming 360 mountain indoor-outdoor spa ",
    "titleIcon":
        "assets/icons/chalet/nearBy/SaintGervaisMontBlancThermalSpa.png",
    "walkType": "Saint-Gervais Mont-Blanc Thermal Spa",
    "walkLimit": "4 hours",
    "detail": "\nUne panoramique expérience de spa charmante au coeur des montagnes…"
        "\n\nSituée à la gorge du Bonnant en plein air aux montagnes, ces thermes ci sont un avant-goût du paradis."
        "\n\nLes minéraux aux eaux thermales sont cicatrisants, apaisants et fortifiants; ils vous rajeunissent. Il y a plusieurs espaces de relaxation et bains minéraux inclus dans chaque séance au spa."
        "\n\nComplétez votre expérience se détendent au chambre marocain, ou, si vraiment vous avez envie de vous faire plaisir, essayez une de ses massages sensationnelles (supplément)."
        "\n\nDe toute façon, assurez-vous de visiter ces thermes et découvrez de ce qu’on parle. C’est une histoire qui englobe plus de 6500 ans d’une eau thermale et ses propriétés réparatrices."
        "\n\nComme ils disent, c’est claire, ‘Réservation conseillée’"
        "\n\n04 50 47 54 57"
        "\n\nTarifs: 2h expérience de spa - weekend (17h45 à 19h45)"
        "\nAdulte: 42€"
        "\nEnfant: 10 to 15€."
        "\nDu Lundi de 10h00 à 21h00"
        "\nDu Mardi au Jeudi de 10h00 à 19h00"
        "\nDu Mardi au Jeudi de 10h00 à 19h00"
        "\nLe Dimanche de 10h00 à 20h00"
        "\n(Dernière séance 2 heures avant la fermeture)"
        "\n\nThe Restorative Alpine Remedy."
        "\n\nOpen year-round, this is the ultimate post-hike or post-slopes relaxation session."
        "\n\nNestled in the basis of the Bonnant valley, the thermal spa offers 360 panoramic views of the surrounding mountains. Relax your muscles after a long day under an alpine starry sky."
        "\n\nInside there are different relaxation spaces and mineral baths and steam rooms included in each two hour session."
        "\n\nThe best session for me is the evening Nocturnes which is also the cheapest, what’s not to like?!"
        "\n\nComplete your restoration in the Moroccan-Turkish chamber. They can do massage and treatments too."
        "\n\nBook your session in advance to avoid disappointment."
        "\n04 50 47 54 57"
        "\n\nEnjoy 10% off as guests of Chalet F’Net. Adult Nocturnes sessions: from 32 euros. Children are allowed twice a week: 10 to 15€.",
  },
  {
    "id": "4",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "assets/icons/chalet/nearBy/franceflag.png",
    "subIcon2": "assets/icons/chalet/nearBy/unitedkingdomflag.png",
    "title": "Brasserie du Mont Blanc",
    "subTitle": "Mmmm…. délicieux",
    "titleIcon": "assets/icons/chalet/nearBy/BrasserieduMontBlanc.png",
    "walkType": "Brasserie du Mont Blanc",
    "walkLimit": "Half day",
    "detail": "\nBrasserie du Mont Blanc,"
        "\n25 Avenue du Mont d’Arbois,"
        "\n74170 Saint-Gervais-les-Bains, France"
        "\n\nBien que les possibilités de sorties dans notre pittoresque Saint-Gervais puissent sembler minces, je trouve qu’une visite à la chaleureuse Brasserie MB ne manquera pas d’égayer tous les soirs."
        "\n\nUn restaurant avec une cuisine de montagne authentique et un personnel joyeux, situé au bas de la Route de Pararion est difficile à refuser. Alors non!"
        "\n\nRecommandation personnelle (pour ceux qui ont un appétit sain), qui selon moi fait le déplacement: Le Burger de Mont-Blanc + bacon et fromage, servi avec de copieuses frites. Pas pour les végétaliens. ;)"
        "\n\nWhilst opportunity for outings in our picturesque Saint-Gervais may seem slim, I find that a visit to the hearty Brasserie MB is bound to brighten any evening."
        "\n\nA restaurant with authentic mountain food and cheerful staff, located at the bottom of la Route de Prarion is hard to refuse. So don’t!"
        "\n\nMy personal recommendation, for those with a healthy appetite, is: The Burger de Mont-Blanc + bacon and cheese, served with copious amounts of fries. Not for vegans ;)",
  },
  {
    "id": "5",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Casa Cosy",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/CasaCosy.png",
    "walkType": "Casa Cosy",
    "walkLimit": "Day trip",
    "detail": "\nCasa Cosy,"
        "\n100 Rue de la Poste,"
        "\n74170 Saint-Gervais-les-Bains,"
        "\nFrance"
        "\n\nInfos@: https://lacasacosy.fr/en/"
        "\n\nThis little place feels like Europe, in a great way."
        "\n\nThey serve some darn good pizza here."
        "\n\nThe menu may be compact, but we can understand this, as the restaurant is committed to providing homemade pizzas with local fresh produce."
        "\n\nVegetarian-friendly options. Vegan possible too."
        "\n\nAs the name of the establishment suggests, you can be guaranteed an intimate setting and a homey meal."
        "\n\nSituated in the lower town, this is a reliable and fun place to go to if you’re returning to Saint Gervais from a day out of town. ",
  },
  {
    "id": "6",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "assets/icons/chalet/nearBy/franceflag.png",
    "titleIcon2": "assets/icons/chalet/nearBy/unitedkingdomflag.png",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Via Ferrata",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/ViaFerrata.png",
    "walkType": "Via Ferrata",
    "walkLimit": "Day trip",
    "detail": "\nSi un jour vous visitiez Samoëns, il y a aussi une VIA FERRATA proche à Sixt Fer-à-Cheval."
        "\n\nCe sentier de montagne, disponible en trois niveaux de difficulté, vous permet de grimper le long des falaises des gorges de Bonnant."
        "\n\nTous les alpinistes seront ravis de participer a cette combinaison d’escalade, de passerelles népalaises, d’échelles, de passerelles rondins, de poutres et de ponts de singes."
        "\n\nEn tant que personne qui a suivi cette voie moi-même, je recommanderais certainement cette excursion divertissante mais stimulante aux touristes de Saint Gervais."
        "\n\nCependant, cette invitation s’adresse uniquement aux personnes avec un degré d’expérience dans le domaine de l’escalade, car elle peut être technique et parfois intimidante."
        "\n\nSince writing this there has been a new opening of via ferrata in St Gervais. We haven't tried it but i’m sure you can expect a similar experience in both."
        "\n\nIf you’re planning on visiting Samoens one afternoon, there is also a super VIA FERRATA nearby in Sixt Fer-a-Cheval."
        "\n\nThis mountain pathway, available with three difficulty levels, enables you to scale across the cliffs of the Bonnant gorge."
        "\n\nMountaineers will delight in this combination of rock climbing, Nepalese bridges, ladders, log walkways, beams and monkey bridges."
        "\n\nAs someone who has completed this journey myself, I would definitely recommend this as a fun yet challenging excursion, but only to someone who has SOME climbing experience since it can be quite technical and daunting at times."
        "\n\nYou will find very helpful guides in the Office de Tourisme in town who will give you climbing lessons: there is a climbing wall too.",
  },
  {
    "id": "7",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Top towns to visit",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/Toptownstovisit.png",
    "walkType": "Top towns to visit",
    "walkLimit": "Day trip",
    "detail": "\nSallanches:"
        "\nDrive from the Chalet: 0h25 min"
        "\nYou can hike, play water sports, and from a cultural standpoint you can discover Baroque churches, chapels and oratories. The town also has the nearest cinema and bowling alley."
        "\n\nAnnecy:"
        "\nDrive from the Chalet: 1h15 min"
        "\nStunning medieval city with lake"
        "\n\nSamoens:"
        "\nDrive from the Chalet: 1h"
        "\nWe lived here for 4 years - nice medieval architecture and great stopping off place to ski the Grand Massif."
        "\n\nPretty, but getting too discovered. Gateway to Sixt-Fer-à-Cheval, which is a pretty stone village at the end of the road bounded by the Cirque. A circle of mountains which soar steeply around you impressively. There’s a Via Ferrata too."
        "\n\nSeveral great concerts in Sixt in the summer and a freezing cold lac bleu to swim and cool down in. Nice views from the refuge at the Col de la joux plane, Refuge la golesses and Bol de gers - all of which you can ride or drive to in the summer and forms the longest vertical off piste descent in somewhere - seriously amazing though I don’t think I will be doing it again."
        "\n\nChamonix:"
        "\nDrive from the Chalet: 0h30 min"
        "\nA classic destination for the young sporty mountain crowd and those shopping for their chalets. Great for a meal out or shopping."
        "\n\nMegève:"
        "\nEver beautiful, I worked here when I was twenty and it holds many memories of snowy sculptures and midnight masses before the medieval church."
        "\n\nVery smart shops and restaurants, all very expensive, but it’s a place to have fun, to look at bling and fur, and there are still a few places for hot choc sur place…"
        "\n\nYou can also travel up to the altiport for a couple of restaurants but avoid chez nano! Great views.",
  },
  {
    "id": "8",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "assets/icons/chalet/nearBy/franceflag.png",
    "titleIcon2": "assets/icons/chalet/nearBy/unitedkingdomflag.png",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Skijöring",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/Skijöring.png",
    "walkType": "Skijöring",
    "walkLimit": "Day trip",
    "detail": "\n(“Ski-Driving” → Alpine Water-Skiing)"
        "\n\nLe ski attelé/ski joëring est une discipline sportive alliant le ski et un attelage animal. Il se pratique avec un cheval attelé qui tire le"
        "\nskieur. Tracté par de gentils chevaux, appréciez un ski hors des sentiers battus qui reste respectueux de l’environnement."
        "\n\nDu pas au galop, des sensations inoubliables y sont garantis! Balades encadrées, parcours ludiques, initiation inclus… L’activité ski joëring est accessible à toute personne (de 8 à 77 ans) capable d’emprunter un téléski."
        "\n\nNous vous proposons également des randonnées équestres hivernales, dans la neige, sur les chemins forestiers."
        "\n\nAttention: Les skis seront fournis, mais il faudra apporter vos propres chaussures de ski Disponible uniquement sur réservation!"
        "\nTARIFS → Adulte : à partir de 50 €."
        "\n\nHarness skiing / ski joëring is a discipline which combines skiing and animal harnessing ; a horse pulls the skier. Discover a new mode of transport… Experience the thrill of being pulled by steady steed and  skiing off the (eco-friendly) beaten track."
        "\n\nFrom walk to canter, an adrenaline-rush is guaranteed. Supervised walks, fun routes and  introduction (to show the ropes) included."
        "\n\nThis activity is available for anyone (aged 8-77) capable of using a drag/button lift. The place also offers winter horse treks through the forest (in the snow)."
        "\n\nPlease note: Skis will be provided, but you NEED to bring your own ski boots. Only available by reservation!"
        "\nPRICES - Adult: From 50 euros.",
  },
  {
    "id": "9",
    "headerIcon1": "assets/icons/chalet/nearBy/franceflag.png",
    "headerIcon2": "assets/icons/chalet/nearBy/unitedkingdomflag.png",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Cable car up the Aiguille du Midi",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/CablecaruptheAiguilleduMidi.png",
    "walkType": "Cable car up the Aiguille du Midi",
    "walkLimit": "HDay trip",
    "detail": "\nLe pas dans le vide / Step into the void"
        "\n\nL’aiguille du Midi se situe dans le massif du Mont-Blanc. Culminant à 3 842 mètres, elle est la plus haute des aiguilles de Chamonix."
        "\n\nL’aiguille est accessible en téléphérique depuis Chamonix. Là-haut, vous pouvez profiter d’une vue panoramique sur les Alpes françaises, suisses et italiennes depuis des terrasses aménagés."
        "\n\nLes personnes les plus intrépides, comme moi, tenteront de marcher sur le sol en verre qui s’étend sur un précipice de 1000m."
        "\n\nAstuce: n’oubliez pas d’apporter votre appareil photo, les vues donnent envie de pratiquer votre photographie amateur."
        "\n\nNotez: le trajet jusqu’à Chamonix depuis le chalet dure un peu plus d’une demi-heure (voiture).  Le funiculaire même prend une demi heure pour monter et il y a souvent des queues longues. C’est aussi le départ pour la descente de la vallée blanche."
        "\n\nThe Aiguille du Midi is a 3,842-metre-tall mountain in the Mont Blanc massif."
        "\nIt can be directly accessed by a cable car from Chamonix that takes visitors up to terraces which offer a 360° view of the French, Swiss and Italian Alps."
        "\n\nBraver folk, like me, will step onto a glass floor which spans over a 1000m precipice."
        "\n\nTop Tip: Remember to bring your camera. The stunning views offer a perfect opportunity to practice amateur photography!"
        "\n\nThe journey from the Chalet to Chamonix is just over 30 minutes long."
        "\nThe Aiguille can be a busy tourist location, prepare for queues in peak season."
        "\n\nThis is also the drop off point for the  famous Valley Blanche ski run."
        "\n\nTarifs/ Prices:"
        "\nTicket Famille/Family pass: 201.60 €"
        "\n(Aller- retour) Round-trip ticket for adult: 65 €"
        "\n(Aller- retour) Round-trip ticket for a child : 55.50 €."
        "\nFree entry for children < 5 years."
        "\nChild entry valid for 5 - 14 years.",
  },
  {
    "id": "10",
    "headerIcon1": "assets/icons/chalet/nearBy/franceflag.png",
    "headerIcon2": "assets/icons/chalet/nearBy/unitedkingdomflag.png",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Ride the Tramway du Mont Blanc Summer and winter",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/RidetheTramwayduMontBlanc.png",
    "walkType": "Ride the Tramway du Mont Blanc Summer and winter",
    "walkLimit": "Day trip",
    "detail": "\nInfos @: "
        "\nhttps://www.montblancnaturalresort.com/en/tramway-montblanc"
        "\n\nLe Fayet / Saint Gervais"
        "\nAccés facile depuis le chalet/ Easy access from Chalet."
        "\n\nEmpruntez ce train à crémaillère avec vue unique sur les Aiguilles de Chamonix et les Dômes de Miage, pour accéder au plateau de Bellevue et découvrir de belles promenades à faire en famille."
        "\n\nPoursuivez en train jusqu’au Nid d’Aigle pour admirer la vue sur le glacier de Bionnassay. Ne manquez pas le superbe panorama sur le massif du Mont-Blanc et le spectacle des alpinistes au départ de l’ascension du Mont-Blanc."
        "\n\nRemarque:"
        "\nOubliez votre acrophobie!"
        "\nAccès adapté à tous les âges."
        "\nApportez votre appareil photo et une bouteille d’eau. Crème solaire et lunettes de soleil sont également recommandés par temps chaud."
        "\n\nTake this one hour train-journey to the Bellevue Plateau up a rickety railway track and discover the inspiring peaks of Chamonix and the Dômes de Miage."
        "\n\nAt the top you can choose between many fun and adventurous hikes, something to take advantage of when with the family. Resume your trip until you reach le Nid d’Aigle for a splendid view of the Bionnassay Glacier. Don’t miss the spectacular panorama over the Mont-Blanc Massif."
        "\n\nTop tips:"
        "\nForget about your acrophobia!"
        "\nSuitable access for all ages. Bring your camera and a bottle of water. Sun cream and sunglasses are also recommended for sunnier season!",
  },
  {
    "id": "11",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Refuge du Tornieux",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/RefugeduTornieux.png",
    "walkType": "Refuge du Tornieux",
    "walkLimit": "Day trip",
    "detail": "\nRefuge du Tornieux."
        "\n\nPiste de luge jusqu’à Sallanches!"
        "\n\nPay a visit to this authentic alpine chalet built in 1892 turned charming mountain hut."
        "\n\nA fun afternoon is going up"
        "\nthere in a caterpillar tractor and sledging down! A real laugh. Suitable for children."
        "\n\nAs winters get milder, it gets harder and harder to sledge down as Sallanches is quite low, it is the town between Megève and St Gervais and this is just above but we think it’s still really worth going up there and having a fondue or even a sleepover."
        "\n\nBook this one."
        "\n\nInfos @: http://www.refuge-tornieux.com/acces/",
  },
  {
    "id": "12",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Say Skiiiii: Ski back home",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/SaySkiiiiiSkibackhome.png",
    "walkType": "Say Skiiiii: Ski back home",
    "walkLimit": "Day trip",
    "detail": "\nChalet F’net is perched on the Prarion mountain with views out over dominion valleys."
        "\n\nAdventurously inclined skiers and boarders can access the Les Houches ski domain without driving (or at least without car driving) on our doorstep."
        "\n\nThis cute ski area includes our favourite restaurants La Tanière, La Cha and Les Vieilles Luges."
        "\n\nYou can also easily access St Gervais, St Nicholas and Les Contamines within a short drive (5-20 mins)."
        "\n\nBeing based at our chalet makes for a very special ski-in which is a lot of fun, but it is weather-dependent and not for a beginner: you come back to us off piste down a track or over the bdefunct pistes."
        "\n\nIf the snow is good this can work. If you don’t fancy it too much, we will pick you up and you will end the day as you begin it – being chauffeured from the drag lift station down the twisty path aboard our caterpillar-mobile."
        "\n\nThere is quite a sneaky kick back on the Les Plancerts drag lift, which is definitely not ideal for debutants."
        "\n\nOtherwise, skiing Les Houches is very family friendly and you can access the main chair lifts and gondolas by car. Happily, Les Houches is considerably less frequented than other busier resorts nearby."
        "\n\nFunnily enough, this little, local resort hosts the world famous Kandahar Alpine Slalom Skiing World Cup every two years."
        "\n\nLes Houches gives you a charming bowl with tree-fringed pistes and the most lovely 360 degree views of the Chain d’Aravis mountains."
        "\n\nThe icing on the cake is the fantastic view of the Mont Blanc and Dents du Midi. You are crazily close to those monoliths."
        "\n\nOverall, Les Houches is a very manageable resort where you can let your kids go and find a bit of fun and meet back up for a cosy chocolat viennois in one of the sweet piste-side restaurants.",
  },
  {
    "id": "13",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Intel on our local ski resorts",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/Intelonourlocalskiresorts.png",
    "walkType": "Intel on our local ski resorts",
    "walkLimit": "Day trip",
    "detail": "\nST GERVAIS, MEGEVE, LES CONTAMINES"
        "\n\nThe connected domaine skiable for St Gervais and Megève is widely fun and extensive, one of the largest in France."
        "\n\nFrom it you can access the villages of Saint Gervais, Saint Nicolas de Veroce, Combloux and La Giettaz to name but a few."
        "\n\nThe ski resort stretches across three mountains: Rochebrune, Mont d’Arbois and Jaillet."
        "\n\nThere are 263km of pistes and the highest run is at 2353m. Great views of the Mont Blanc, mostly in the tree line, good back country and a good chance of snow."
        "\n\nSt Gervais is more economical; Megève, former playground to the Rothschilds, a place to be seen. Slopeside in Megève will be dear but good."
        "\n\nLes Contamines has the best snow but more hot dogs and chips in terms of catering. Nothing wrong with that."
        "\n\nSt Gervais is a happy medium, with good skiing and decent eats at La Ravière (you can also hike there), Sous -Les-Freddys, and the ubiquitous Folie Douce for a late afternoon session of costumed tunes and dancing on tables.",
  },
  {
    "id": "14",
    "headerIcon1": "",
    "headerIcon2": "",
    "titleIcon1": "",
    "titleIcon2": "",
    "subIcon1": "",
    "subIcon2": "",
    "title": "Which is the lift pass for you?",
    "subTitle": "",
    "titleIcon": "assets/icons/chalet/nearBy/Whichistheliftpassforyou.png",
    "walkType": "Which is the lift pass for you?",
    "walkLimit": "Day trip",
    "detail": "\nOption 1. Local Les Houches"
        "\nThe ‘Milk Tray’ Option."
        "\n\nKeep it simple with the winning formula of Les Houches, you don’t even have to go to the office. The lift boys will let you up the Plancerts, lift just above Chalet F’net, so that you can ski down to the Bellevue lift office, supposing you have your ski gear at the chalet on the first morning."
        "\n\nOption 2. Evasion Mont Blanc"
        "\nDIVERSE & LOCAL"
        "\n\nIf you are buying a weekly or season pass, you could opt for Evasion Mont Blanc which covers St Gervais, Megève, Les Contamines and St Nicholas too. You can order online with a small discount. Comfortable, lots of skiing, lots of restaurants, lovely scenery. Plenty to keep you going for a week."
        "\n\nPerhaps the biggest shame of this is that this pass does not cover our Les Houches which belongs to the Chamonix Mont Blanc Unlimited pass."
        "\n\nOption 3. Chamonix Unlimited"
        "\nALL IN."
        "\n\nThe Chamonix Mont Blanc Unlimited pass encompasses CHAMONIX, VALLEE BLANCHE, COURMAYEUR and more…! If you like the sound of this, the flexibility to ski all including Les Houches and Chamonix among others, you should go for the Mont Blanc Unlimited pass. Unsurprisingly, since this one is the über pass, it is the most expensive."
        "\n\nThis pass is probably best if you have at least ten days, are energetic, and want to pack a lot of touring into your holidays. This would cover you for adventures like popping up the James Bond funicular to visit the Aiguille du Midi station, from where you can just drink in the views or ski the Vallée Blanche."
        "\n\nCham MB Unlimited pass does also include the option of a couple of days in Courmayeur on the Italian side, as well as access to the shuttle bus free or discount on the tunnel if you want to drive. It’s a very different vibe in smart Courmayeur to sporty Cham."
        "\n\n1, 2, 3… Conclusion"
        "\n\nAll three of these options are suitable for all levels of skiers, the choice mainly depends on your allowance for skiing and on how adventurous you want to be."
        "\n\nOption 1. Families, friends, amateurs, all the way up to advanced, can have a great time here. When we go ourselves normally we ski Les Houches, it’s fun, a good price and just on our doorstep. Competent skiers take Les Plancerts from us; beginners/ less confident skiers do the short drive over to the main resort (Bellevue, on the piste map)."
        "\n\nOption 2. Diverse, exciting. "
        "\nSuitable for all levels of skiers, the joint ski domaine between St Gervais-Megève does offers fun for everyone."
        "\nWith this option, St Gervais lift pass would allow you to take the tram up to the top of Les Houches so you could still get the Mont Blanc view and in theory ski home."
        "\n\nOption 3. The adventurous."
        "\nLots of fun to be had, and lots of different scenery to see. A bit more pricey but if you are sporty or outgoing or wanting to explore as much as you can, this might be the option for you. "
        "\nIf you are still unsure, another option is to buy day passes to wherever you would like to be for the day. This works well if you want to see different resorts, and if, say, you wanted to take a day off for rest or another form of activity, such as hiking, climbing, ice-skating, swimming, the list goes on, this is your playground too now.",
  },
];

List beachHouseDoNearBy = [
  {
    "id": "1",
    "title": "Visit Dylan Thomas' writing cabin",
    "subTitle": "Richard Burton’s sonorous words were chewed and honed",
    "titleIcon":
        "assets/icons/beachHouse/doNearBy/VisitDylanThomaswritingcabin.png",
    "walkType": "Visit Dylan Thomas' writing cabin",
    "walkLimit": "3 hours",
    "detail": "\nWelsh bard Dylan Thomas’ most famous work was arguably ’Under Milk Wood’, immortalised by Richard Burton."
        "\n\nThis radio play was set in the fictional Welsh village of Llareggub, after which we named our Beach House."
        "\n\nPoet was never to hear it performed: Dylan wrote it just before his tragic death from alcohol poisoning."
        "\n\nPour yourself a tipple of something warming and listen to the recording on YouTube; settle in for an hour of language as music, phrases composed rather than written."
        "\n\nThe Dylan Thomas Trust has restored his tiny writing shed in Laugharne to its original state. It’s well worth a visit, with a nice and not too taxing walk from the estuary side car park."
        "\n\ndylanthomasboathouse.com",
  },
  {
    "id": "2",
    "title": "Cwlbox food truck on Saundersfoot beach",
    "subTitle": "Al fresco food – just about the opposite of dining al desko",
    "titleIcon":
        "assets/icons/beachHouse/doNearBy/CwlboxfoodtruckonSaundersfootbeach.png",
    "walkType": "Cwlbox food truck on Saundersfoot beach",
    "walkLimit": "1 hour",
    "detail": "\nWe love the open skies and fresh air of Pembrokeshire. Our friends at Cwlbox  have stylishly revamped a former horse-box and set up on the landing ramp down to Saundersfoot beach."
        "\n\nCwlbox rustle up amazing seafood treats: super fresh lobster, bang bang prawns, scampi, dirty fries, home-made sauces and salads."
        "\n\nIf you head down in the morning, you can grab coffee to warm the cockles of your heart for your beach walk."
        "\n\nIf the weather is less clement, then head down to Pembroke Dock where their pop up restaurant is available at Y Gegin foodhall."
        "\n\nhttps://www.cwlbox.com",
  },
  {
    "id": "3",
    "title": "Walk to Harbwr microbrewery in Tenby",
    "subTitle": "Check the tides before you go...",
    "titleIcon":
        "assets/icons/beachHouse/doNearBy/WalktoHarbwrmicrobreweryinTenby.png",
    "walkType": "Walk to Harbwr microbrewery in Tenby",
    "walkLimit": "3 hours",
    "detail": "\nYou can take a bracing walk along the beach to Tenby and top up with a pint at the Harbwr microbrewery. They even have a beer named after our old friend George North, Welsh rugby legend! Be sure to try North Star. You can even pull your own pint here."
        "\n\nCheck the tides before you go unless you fancy getting wet feet and a drowned phone. (We’ve been there. It was squidgy.) The best way to do it is come out of Captain’s Walk on to the main road, turn left, then first left again down the little lane to the beach. And follow the coast along to the right."
        "\n\nWhile you’re there, Tenby is also a great place to get some fresh seafood, head to the little hut down by the harbour. Make sure you see our entry to Tenby in ‘Nearby Walks’ for more things to do on your day out. :)  "
        "\n\nTo find out more about this really hip brewery hangout, check out their website: https://harbwr.wales",
  },
  {
    "id": "4",
    "title": "Recentre yourself at St Brides Hotel",
    "subTitle": "Smooooooth, skin",
    "titleIcon":
        "assets/icons/beachHouse/doNearBy/RecentreyourselfatStBridesHotel.png",
    "walkType": "Recentre yourself at St Brides Hotel",
    "walkLimit": "Half day",
    "detail": "\nA hop skip and not even a jump away from us is St Bride’s hotel."
        "\n\nUnbeknown to the outside world, this hotel has the most lovely spa at the heart of it, with the very finest infinity pool, one in a league of its own above any that I’ve been in or seen, in even more dramatic locations. Seeing the sea water leading out onto sea water is so restful."
        "\n\nThe treatments are great and salty and the wraps are wonderful and combine massage and treatment. I would say - MUST - book ahead. They even have a double massage room which has views out onto the bay - ok it’s the same view as at home but who but a philistine would ever get sick of that. { must specify if you want this}."
        "\n\nTreatments around 60 pounds , if you take two you get the thermal suite free. Otherwise it is £25. Still worth it. "
        "\n\nhttps://www.stbridesspahotel.com",
  },
  {
    "id": "5",
    "title": "Learn to Surf",
    "subTitle": "Surf surf babay",
    "titleIcon": "assets/icons/beachHouse/doNearBy/LearntoSurf.png",
    "walkType": "Learn to Surf",
    "walkLimit": "Day trip",
    "detail": "\nSome great places to surf near us are New Gale, Manor Bier and Freshwater East."
        "\n\nAward-winning Outer Reef Surf School is the place to start. All levels are welcome. All levels will have a great time!"
        "\n\nVery airly-priced too! (35 pounds for a 2 hour session, all equipment included). This is their website here: https://www.outerreefsurfschool.com "
        "\n\nIf you’re not mad for the surf, this is the place to hang out on a paddle board while you wait for those surfing."
        "\n\nOtherwise, if you don’t really feel like getting your hair wet, you can walk along the beach foraging gorgeous fresh watercress and take in the aromatic wild garlic in season till May."
        "\n\nReunite with the others afterwards and visit the lovely Manorbier castle and warm up in our great-auntie Aggie’s pub before returning home after a fun and complete day out.",
  },
  {
    "id": "6",
    "title": "Our favourite beaches to surf",
    "subTitle":
        "According to the local beach go-ers (that aren’t local and are from actually from London)",
    "titleIcon":
        "assets/icons/beachHouse/doNearBy/Ourfavouritebeachestosurf.png",
    "walkType": "Our favourite beaches to surf",
    "walkLimit": "Day trip",
    "detail": "\nPembrokeshire provides the greatest beaches for surfing, whether it’s your first time or if you’re an experienced surfer looking for great waves but fewer people. With over 50 beaches in Pembrokeshire alone, there is one for everyone’s different surfing experience."
        "\n\nWhitesands Beach is located on the Pembrokeshire coast via St David’s. This beach is perfect for mixed abilities in surfing experience. Whitesands is described as one of the best surfing beaches and also one of the most popular with tourists. The white water on the beach is perfect for beginners and improvers. On bigger wave days the rip at the North end of the beach is a great surf for intermediate/ advanced surfers. At certain times of the year the beach offers surf school lessons and equipment, a café, shop and ice cream. The lifeguards are on the beach May-September and the beach is close to St David’s, Solva and Roch which are great places for little village shops, restaurants and of course fish and chips!"
        "\n\nNewgale Beach is an award-winning blue flag beach that stretches for 2 miles along the beautiful Pembrokeshire coast, with its golden sands and a strip of pebbles Newgale is one of the most accessible beaches in Pembrokeshire. The south west facing beach has a consistent surf with great swells and easterly winds that are not too powerful which make the water and waves perfect for all levels of surfers. The tide at Newgale can reach to the pebbles so for beginner/ improve surfers its best to avoid the high tides. Newgale beach provides surf school, hire of equipment and a couple of cafes and a pub close to the beach."
        "\n\nFreshwater West Beach is one of Wales best surfing beaches for intermediate/advanced surfers. The beach is surrounded by sand dunes and the surf is consistent, the waves can be fast and powerful with strong underwater currents in places. The beach surf is not for beginners however the beach offers mobile surf school for children and adults. By the side of the water a multi-award-winning gastro beach shack, Café Mor, offering amazing foods that are locally sourced from water and land. ",
  },
  {
    "id": "7",
    "title": "Foraging with Matt",
    "subTitle": "Simply yum",
    "titleIcon": "assets/icons/beachHouse/doNearBy/ForagingwithMatt.png",
    "walkType": "Foraging with Matt",
    "walkLimit": "Day trip",
    "detail": "\nOMG is there nothing this guy can’t do. Our very own Dylan. Check his exquisite instagram to see what we mean @fishingandforagingwales."
        "\n\nFish, take beautiful photos and spout the most incredible zeitgeist about the plants and weeds that his grandparents used to eat and pic, goodness me… Hunt sea bass and let them go…"
        "\nFeast upon his birch syrups dribbled over the preserved fruits of the earth. "
        "\n\nBeautifully preserved, presented, and cooked a many-course, eye-opening meal - the foraging expedition with Matt Powell is not to be missed."
        "\n\nWe haven’t been fishing with him yet but he we hear he launches into the bracing Welsh waves with vigour and is in love with his fish. Never keeps them unless to eat . It takes 5 days to cure a sea bass. Didn’t know it was ill. Choose one of his three menus or invite him to cook over at Llaregubb. Book in advance! Busy bee."
        "\n\nMany plant-based, vegetarian and vegan courses"
        "\n\nfishandforagingwales.co.uk",
  },
  {
    "id": "8",
    "title": "Visit Caldey Island",
    "subTitle": "One historic tiny gem of an island",
    "titleIcon": "assets/icons/beachHouse/doNearBy/VisitCaldeyIsland.png",
    "walkType": "Visit Caldey Island",
    "walkLimit": "Day trip",
    "detail": "\nMake sure you catch a quick boat ride from Tenby out to Caldey Island - the serene island home of Cistercian monks."
        "\n\nThis treasure of an island has its own quite special history spanning over 1500 years. Remarkably, monks have inhabited the island since the 6th C. Today, the heart of the island is Caldey Monastery & Abbey, built between 1910 and 1912 by touring Anglican Benedictine monks."
        "\n\nAttend the lunchtime monastic service, breathe the atmosphere of the abbey, and then head to the beach. The splendid isolation of Caldey’s beaches and the views looking back to pastel-coloured Tenby are well worth the trip."
        "\n\nBe sure to bring home monk-made Caldey island chocolate, it is out of this world."
        "\n\nBoats run regularly between Tenby and Caldey Island Mon-Sat (mid-May to mid-Sept). Dogs are welcome too on a lead.",
  },
  {
    "id": "9",
    "title": "Fishing Excursions from Saundersfoot Harbour",
    "subTitle": "A day out mackerel fishing & beach sunset dinner",
    "titleIcon":
        "assets/icons/beachHouse/doNearBy/FishingExcursionsfromSaundersfootHarbour.png",
    "walkType": "Fishing Excursions from Saundersfoot Harbour",
    "walkLimit": "Day trip",
    "detail": "\nIf you’re looking out to sea and pondering a fun family activity one afternoon, just head down to Saundersfoot Harbour."
        "\n\nWe recommend an afternoon's mackerel fishing with a super friendly little company called Four Brothers Fishing Trip."
        "\n\nBoats depart at regular 2 hour intervals for these fishing trips (10, 12, 2, 4). These gentle adventures are suitable for all ages and any level of experience (dogs are welcome too!). Everything included. Children @ 15, adults @ 20. Loads of fun, well worth the money (we all managed to catch our dinner)."
        "\n\nWhen we return after a good day out at around 5pm we’re ready for a swim and a sunset dinner. Bring your catch to the secret beach and coves to the right of the harbour. Pick up some great chips to have with dinner from the excellent local chippie just by the harbour, head to the beach with a bottle of wine. Grill the mackerel and enjoy a secret and well-earned sunset dinner."
        "\n\nTop tips: remember to bring a backpack with swimmers, towels, cash, cutlery, plates, and matches to spark the fire."
        "\n\nAlso watch out for the cheeky gulls they will have Mackie’s as soon as you turn your back. ",
  },
];

List beachHouseNearByWalk = [
  {
    "id": "1",
    "title": "Saundersfoot to Tenby Walk",
    "subTitle": "A famous five style adventure",
    "titleIcon":
        "assets/icons/beachHouse/nearBy/SaundersfoottoTenbyWalk.png",
    "walkType": "Saundersfoot to Tenby Walk",
    "walkLimit": "4 hours",
    "detail": "\nSomething very close to our hearts is our childhood walk along the Pembrokeshire Coast Path from Saundersfoot to Tenby. Set out when the tide is low & you'll be able to do a lot of this walk along the beach; if you plan this well you can do beach all the way! The coast path is also gorgeous, something about the perspective and stunning views out to sea."
"\n\nThe total walk’s about 1 hour if you're quite speedy up to 1.5 hours if you're walking more leisurely. While it is mostly quite easy-going and really okay for most ages, we would say it helps to enjoy walking! This really is a mini adventure."
"\n\nWhen you spot lovely pastel coloured houses and arrive into the walled city of Tenby, celebrate with a refreshing fruit juice, or warm up with a cup of Pembrokeshire tea at Cafe 25, run by our Uncle John. Then wander through the old town, taking in history and charm,  and window-shopping along the way at the boho shops and higgledy-piggledy book-shops."
"\n\nPick up some freshly baked bread at Loafley on Upper Frog Street [if you haven’t yet tried the sourdough option at our own home baked @hugtasty], cakes, or welsh cakes on the street near Guy Manning’s art gallery [07903 782515 www.guymanning.co.uk] where you can even pen a quick watercolour under his guidance."
"\n\nThen go forth and end your day in style, pouring yourself a pint of North Star at the microbrewery, named after friend and rugby international George North, whose stag night we threw at our own RKB."
"\n\nReturning home from Tenby by taxi (£5-10) can be welcome at the end of a long day but if you are really brave and considerate of incoming tides then you have the option of walking back via coast too.",
  },

  {
    "id": "2",
    "title": "Dylan Thomas’ birthday walk, Laugharne",
    "subTitle": "Along the heron priested shore of Laugharne’s estuary",
    "titleIcon": "assets/icons/beachHouse/nearBy/DylanThomasbirthdaywalkLaugharne.png",
    "walkType": "Dylan Thomas’ birthday walk, Laugharne",
    "walkLimit": "Day trip",
    "detail": "\nLaugharne is the home of drunken genius poet Dylan Thomas. On his thirtieth birthday, he walked along the estuary from his tiny writing cabin, perched over the sea, into the woods on the hill. Most people just go for a nice walk, but not our Dylan."
        "\n\nHe immortalised his birthday stroll in the iconic poem in October, famous for his characteristic, bittersweet blend of joy in the crunchiness of"
        "\nlanguage and cynicism (the opening line reflects his glass half-empty view of life – “It was my thirtieth year to heaven”)."
        "\n\nPark up in Laugharne village and turn right along the “heron-priested shore” and up the hill. Thirsty walkers will want a pint after at Brown’s Hotel, Dylan’s favourite brown wood boozer, just up the road from the car park. These days it’s a bit more gentrified."
        "\n\nBut keep it steady with the beers if you’re driving, boyo."

  },
  {
    "id": "3",
    "title": "Walk the beach to Coast restaurant",
    "subTitle": "Stunning floor to ceiling windows with ocean views",
    "titleIcon":
    "assets/icons/beachHouse/nearBy/WalkthebeachtoCoastrestaurant.png",
    "walkType": "Walk the beach to Coast restaurant",
    "walkLimit": "3 hours",
    "detail": "\nIt’s a twenty minute power-walk, or a more sedate half an hour affair to Coast, the best restaurant in West Wales."
        "\n\nMeals begin in style with their homemade foaming butter and crunchy breads. This place is heading for a Michelin star in short order, and there are stunning views across"
        "\nSaundersfoot Bay."
        "\n\nTake a pair of sunglasses, the light can be dazzling. You can pretend to be Richard Burton and Elizabeth Taylor after recording."
        "\n\nSo… it’s not cheap. (OK – it’s pretty damn pricey), but this is definitely the place for a blow-out meal, the meal-stone, that will cap off your Welsh adventure. The Sunday lunch set menu is a more reasonable proposition."
        ,
  },
  {
    "id": "4",
    "title": "The Barafundle Boogie",
    "subTitle": "Stunning floor to ceiling windows with ocean views",
    "titleIcon":
    "assets/icons/beachHouse/nearBy/TheBarafundleBoogie.png",
    "walkType": "The Barafundle Boogie",
    "walkLimit": "Day trip",
    "detail": "\nOne of our personal favourite Pembrokeshire beach hangouts is Barafundle Bay."
        "\n\nSure, it may be a little further into Pembrokeshire but it’s an incredibly interesting walk, leading you down from the car park (or the Pembrokeshire coast path if you fancy a walk there), into a few fields and through an stunning marina-quarry - Stackpole, a great place to do some jumping from heights or maybe just the kids."
        "\n\nIf you fancy cake and coffee, or the loo, before continuing your journey, be sure to stop off at the boathouse tea room. It’s very well-kept, and has a delightful selection of sweet and savoury treats to ease the hunger ahead of your adventure!"
        "\n\nFrom the boathouse, continue up the trodden path and you will arrive at Barafundle bay. This place, unknown to most, is our secret with you."
        "\n\nIf you’re looking for a quiet spot you continue up and over the opposite side of the beach, finding rest betwixt the coves & stone arches."
        ,
  },


];
