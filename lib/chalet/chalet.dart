
import 'dart:io';
import 'dart:typed_data';
//import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:typed_data';
import 'package:Chillderness/chalet/FullPdfViewerScreen.dart';
import 'package:Chillderness/chalet/chaletNearWalks.dart';
import 'package:Chillderness/chalet/chaletdoNearBy.dart';
import 'package:Chillderness/res/color.dart';
import 'package:Chillderness/res/urlLauncher.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';


import 'beginingNavigation.dart';
class chalet extends StatefulWidget {
  @override
  _chaletState createState() => _chaletState();
}

class _chaletState extends State<chalet> {
   String _documentPath = 'assets/pdfFile/ChaletYourGuide.pdf';
//   Future<String> prepareTestPdf() async {
//     final ByteData bytes =
//     await DefaultAssetBundle.of(context).load(_documentPath);
//     final Uint8List list = bytes.buffer.asUint8List();
//     final tempDir = await getTemporaryDirectory();
//     final tempDocumentPath = '${tempDir.path}/$_documentPath';
//
//     final file = await File(tempDocumentPath).create(recursive: true);
//     file.writeAsBytesSync(list);
//     return tempDocumentPath;
//   }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                    color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                  text: TextSpan(
                    text: "Welcome to the Chalet,",
                    style: styles.PlayfairDisplayBoldItalic(
                      fontSize: size.convert(context, 19)
                    )
                  ),
            ),
                ],
              ),),
            SizedBox(height: 5),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "It's great to have you staying with us",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 16,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/chalet/chaletMenu.png",),),
            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/mapIocn.png",),),
            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                UrlLaunch.OpenGoogleMap("https://goo.gl/maps/31qY5ixwWZ8T4yGw8");
//                  Navigator.push(context, PageTransition(child: beginingNavigation(),
//                  type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                  color: buttonColor,
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Navigate to the Chalet",
                        style: styles.PlayfairDisplayBlack(
                          fontSize: size.convert(context, 12,),
                          color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),


            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, PageTransition(child: chaletNearWalks(),
                    type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "View nearby walks",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, PageTransition(
                  child: FullPdfViewer(),
                  type: PageTransitionType.leftToRight
                ));
//                prepareTestPdf().then((path) {
//                  print("this is file path"+path);
//                  Navigator.push(
//                    context,
//                      PageTransition(
//                        child: FullPdfViewerScreen(path),type: PageTransitionType.leftToRight
//                      )
////                    MaterialPageRoute(
////                        builder: (context) => FullPdfViewerScreen(path)),
//                  );
//                });
                //UrlLaunch.Viewtheguide();
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "View the guide",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                UrlLaunch.BrowseLocalOffers();
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Browse Local Offers",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                Navigator.push(context, PageTransition(child: chaletdoNearBy(),
                    type: PageTransitionType.leftToRight));
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "View fun activities nearby",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){
                UrlLaunch.VisittheInstagram();
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Visit the Instagram",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            GestureDetector(
              onTap: (){

                UrlLaunch.Reviewus();
              },
              child: Container(
                width: size.size362(context, 362),
                margin: EdgeInsets.symmetric(horizontal: 35),
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Review us",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12,),
                            color: buttonTextColor
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_forward_ios,
                      color: buttonIconColor,
                      size: IconHeight,
                    )
                  ],
                ),),
            ),

            SizedBox(height: 10,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/colorLogo.png",),),
            SizedBox(height: 10,),
          ],
        ),),
      ),
    );
  }
}
