import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
class FullPdfViewer extends StatefulWidget {
  @override
  _FullPdfViewerState createState() => _FullPdfViewerState();
}

class _FullPdfViewerState extends State<FullPdfViewer> {
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  PDFDocument _document;
  bool isLoading = true;
  loadFromAssets()async{
    print("///////////////////////");
    _document = await PDFDocument.fromAsset("assets/pdfFile/ChaletYourGuide.pdf");
    setState(() {
      isLoading = false;
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadFromAssets();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: PDFViewer(
        CustomPreferredSizeWidget: PreferredSize(
        preferredSize: Size.fromHeight(200),
        child: Container(
          //color: Colors.amberAccent,
          margin: EdgeInsets.symmetric(vertical: 25),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 35),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.arrow_back_ios,
                    color: Colors.black,),
                ),
                Image.asset("assets/icons/logo.png",
                  //width: size.convertWidth(context, 116),
                ),
              ],
            ),
          ),
        )
        ),
          document: _document,
      ),
    );
  }
}

//class FullPdfViewerScreen extends StatelessWidget {
//  final String pdfPath;
//
//  FullPdfViewerScreen(this.pdfPath);
//
//  @override
//  Widget build(BuildContext context) {
//    print("/////////////////////////////");
//    print(pdfPath);
//    print("/////////////////////////////");
//
//    return PDFViewerScaffold(
//        appBar: AppBar(
//          title: Text("Document"),
//        ),
//        path: pdfPath);
////        return Scaffold(
////          body: Column(
////            children: <Widget>[
////              Container(
////                child: PDFView(
////                  filePath: path,
////                  enableSwipe: true,
////                  swipeHorizontal: true,
////                  autoSpacing: false,
////                  pageFling: false,
////                  onRender: (_pages) {
////                    setState(() {
////                      pages = _pages;
////                      isReady = true;
////                    });
////                  },
////                  onError: (error) {
////                    print(error.toString());
////                  },
////                  onPageError: (page, error) {
////                    print('$page: ${error.toString()}');
////                  },
////                  onViewCreated: (PDFViewController pdfViewController) {
////                    _controller.complete(pdfViewController);
////                  },
////                  onPageChanged: (int page, int total) {
////                    print('page change: $page/$total');
////                  },
////                ),
////
////              )
////            ],
////          ),
////        );
//  }
//}