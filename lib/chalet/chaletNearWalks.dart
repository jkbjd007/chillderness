import 'package:Chillderness/detailScreen.dart';
import 'package:Chillderness/items.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/otherScreen/Late-night-arrivals.dart';
import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:flutter/widgets.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class chaletNearWalks extends StatefulWidget {
  @override
  _chaletNearWalksState createState() => _chaletNearWalksState();
}

class _chaletNearWalksState extends State<chaletNearWalks> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "View nearby walks",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 19)
                          )
                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 5),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "Explore god's own country.",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            Container(
                width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: ListView.separated(
                shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemBuilder: (BuildContext context, int index){
                    return GestureDetector(
                      onTap: (){
                        Navigator.push(context, PageTransition(
                            type: PageTransitionType.fade,
                            child: detailScreen(
                              titleIcon: chaletList[index]["titleIcon"],
                              title: chaletList[index]["title"],
                              subTitle: chaletList[index]["subTitle"],
                              detail: chaletList[index]["detail"],

                            )
                        ));
                      },
                      child: Container(
                        child: Stack(
                          alignment: Alignment.bottomCenter,
                          children: <Widget>[
                            Image.asset(chaletList[index]["titleIcon"],
                              width: size.size362(context, 362),),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                                width: size.size362(context, 362),
                              height: size.convert(context, 40),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  color: Colors.black,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(10),
                                  bottomLeft: Radius.circular(10),
                                )
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                Expanded(
                                  child: Text(chaletList[index]["walkType"],
                                  style: styles.PlayfairDisplayBlack(
                                    fontSize: size.convert(context, 12)
                                  ),
                                  ),
                                ),
                                Container(width: 2,
                                  height: size.convert(context, 40),
                                  decoration: BoxDecoration(
                                    color: Colors.black
                                  ),
                                ),
                                Container(
                                  width: 120,
                                  child: Center(
                                    child: Text(chaletList[index]["walkLimit"],
                                      style: styles.PlayfairDisplayBlack(
                                          fontSize: size.convert(context, 16)
                                      ),
                                    ),
                                  ),
                                ),
                              ],),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index){
                    return Container(height: size.convert(context, 10),);
                  },
                  itemCount: chaletList.length ?? 0
              )
            ),
            SizedBox(height: size.convert(context, 10),),
          ],
        ),),
      ),
    );
  }
}
