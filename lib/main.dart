import 'package:Chillderness/beach-house/beachHouse.dart';
import 'package:Chillderness/chalet/chalet.dart';
import 'package:Chillderness/conactNumber.dart';
import 'package:Chillderness/conker/conker.dart';
import 'package:Chillderness/red-kit-barn/red-kit-barn-menu.dart';
import 'package:Chillderness/red-kit-green/red-kit-green-menu.dart';
import 'package:Chillderness/red-kit-red/red-kit-red-menu.dart';
import 'package:Chillderness/res/sizeConfig.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:page_transition/page_transition.dart';
import 'package:url_launcher/url_launcher.dart';
void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    //statusBarBrightness: Brightness.dark,
    //statusBarIconBrightness: Brightness.light,
  ));

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
//    SystemChrome.setPreferredOrientations([
//      DeviceOrientation.portraitUp,
//    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        //primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        //visualDensity: VisualDensity.adaptivePlatformDensity,
          scaffoldBackgroundColor: Color(0Xffffffff)
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("Error in Url ${url}" );
      throw 'Could not launch $url';
    }
  }
  List imageSlider = [
    {
      "id":1,
      "icon":"assets/icons/homePageIcons/startOur.png",
      "url":"https://www.chillderness.co.uk/meadow"

    },
    {
      "id":2,
      "icon":"assets/icons/homePageIcons/viewOurPariners.png",
      "url":"https://www.chillderness.co.uk/partners"
    },
    {
      "id":3,
      "icon":"assets/icons/homePageIcons/bookStay.png",
      "url":"https://www.chillderness.co.uk"
    },
    {
      "id":4,
      "icon":"assets/icons/homePageIcons/contactNumber.png",
      "url":""
    },
    {
      "id":5,
      "icon":"assets/icons/homePageIcons/plantTrees.png",
      "url":"https://www.chillderness.co.uk/trees"
    },
    ];
  List categories = [
    {
      "icon":"assets/icons/homePageIcons/mythology-and-utopia.png",
      "title":"Between mythology and utopia"
    },
    {
      "icon":"assets/icons/homePageIcons/Stuff-of-legends.png",
      "title":"Stuff of legends"
    },
    {
      "icon":"assets/icons/homePageIcons/Surf-in-the-Bahamas-of-Wales.png",
      "title":"Surf in the Bahamas of Wales"
    },
    {
      "icon":"assets/icons/homePageIcons/Fifty-shades-of-green.png",
      "title":"Fifty shades of green"
    },
    {
      "icon":"assets/icons/homePageIcons/Love-conkers-all.png",
      "title":"Love conkers all"
    },
    {
      "icon":"assets/icons/homePageIcons/Chalet-for-old-soaps.png",
      "title":"Chalet for old soaps"
    }
    ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
        child: Column(children: <Widget>[
          SizedBox(height: size.convert(context, 45),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 45),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Image.asset("assets/icons/logo.png",
                //width: size.convertWidth(context, 116),
                ),
              ],
            ),
          ),
          SizedBox(height: 5,),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 45),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text("Surf, Turf, Snow",
                style: styles.PlayfairDisplayBoldItalic(
                  fontSize: size.convert(context, 19)
                ),)
              ],
            ),
          ),
          //SizedBox(height: 10,),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 30),
            child: ListView.separated(
                itemBuilder: (BuildContext context, int index){
                  return GestureDetector(
                    onTap: (){
                      if(index == 0 ){
                        Navigator.push(context, PageTransition(
                            child: ridKitGreenMenu(),
                            type: PageTransitionType.leftToRight
                        ));
                      }
                      if(index == 1 ){
                        Navigator.push(context, PageTransition(
                            child: ridKitRedMenu(),
                            type: PageTransitionType.leftToRight
                        ));
                      }
                      if(index == 2 ){
                        Navigator.push(context, PageTransition(
                            child: beachHouse(),
                            type: PageTransitionType.leftToRight
                        ));
                      }

                      if(index == 3){
                        Navigator.push(context, PageTransition(
                            child: ridKitBarnMenu(),
                            type: PageTransitionType.leftToRight
                        ));
                      }

                      if(index == 4){
                        Navigator.push(context, PageTransition(
                            child: conker(),
                            type: PageTransitionType.leftToRight
                        ));
                      }

                      if(index == 5){
                        Navigator.push(context, PageTransition(
                            child: chalet(),
                            type: PageTransitionType.leftToRight
                        ));
                      }

                    },
                    child: Container(
                      child: Center(
                        child: Stack(
                          alignment: Alignment.bottomRight,
                          children: <Widget>[
                          Container(
                            child: Image.asset(categories[index]["icon"]),),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 5,vertical: 5),

                              child: Text(categories[index]["title"],
                              style: styles.PlayfairDisplayBoldItalic(
                                color: Colors.white,
                                fontSize: size.convert(context, 13)
                              ),),
                            )
                        ],),
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index){
                  return SizedBox(height: 15,);
                },
                itemCount: categories.length ?? 0,
                shrinkWrap: true,
                physics: ScrollPhysics(),

            ),
          ),
          SizedBox(height: 15,),
          Container(
            height: size.convert(context, 150),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Color(0xffe58064)
            ),
            child: ListView.separated(
              itemBuilder: (BuildContext context, int index){
                return GestureDetector(
                  onTap: (){
                    if(imageSlider[index]["id"] == 4){
                      Navigator.push(context, PageTransition(child: conactNumber(),
                      type: PageTransitionType.leftToRight));
                    }
                    else{
                      _launchURL(imageSlider[index]["url"]);
                    }
                  },
                  child: Container(
                    child: Container(
                            child: Image.asset(imageSlider[index]["icon"],
                            width: 200,),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index){
                return SizedBox(width: 1,);
              },
              itemCount: imageSlider.length ?? 0,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              scrollDirection: Axis.horizontal,
            ),
          ),
        ],),
      ),),

    );
  }
}
