import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class beginingNavigation extends StatefulWidget {
  @override
  _beginingNavigationState createState() => _beginingNavigationState();
}

class _beginingNavigationState extends State<beginingNavigation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Beginning navigation to the Red Kite Green Tent",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 27)
                          )
                      ),
                    ),
                  ),
                ],
              ),),

            SizedBox(height: size.convert(context, 10),),
            Container(
              height: size.convert(context, 500),
              padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 15)),
              decoration: BoxDecoration(
                color: boxColor,
                borderRadius: BorderRadius.circular(10)
              ),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Mapbox navigation integrated here,\n"
                      "alternatively this page could\n"
              "redirect straight\n"
              "to google navigation but I would\n"
                  "prefer the first option,",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 15),
                            color: boxTextColor
                          )
                      ),
                    ),
                  ),
                ],
              ),),

          ],
        ),),
      ),
    );
  }
}
