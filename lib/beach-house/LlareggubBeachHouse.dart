import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class LlareggubBeachHouse extends StatefulWidget {
  @override
  _LlareggubBeachHouseState createState() => _LlareggubBeachHouseState();
}

class _LlareggubBeachHouseState extends State<LlareggubBeachHouse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Llareggub Beach House",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 19)
                          )
                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 4),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "Our Guide",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/beachHouse/Latenightarrivals.png",
                width:  size.size362(context, 362),
              ),),
            SizedBox(height: 15,),
            Container(
              width: size.size362(context, 362),
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          style: styles.QuicksandBold(
                              fontSize: 16,
                              color: Colors.black
                          ),
                          text: "Your seaside adventure begins at 4 pm."
                              "\n\nYou will soon be enjoying one of the finest views anywhere."
                              "\n\n‘Llareggub’, named after the literary village in Dylan Thomas’s “Under Milk Wood”, is the second house along the cliff edge from St Brides Spa Hotel. If space permits, park at the low stone wall in front of houses 1-6. If not, and space is often at a premium, please park with consideration where you can. If you have come with more than one car you may need to find a side street nearby. But parking is free near us. :)"
                              "\n\nWhen you arrive, take the elevated bridge to the front door of Llareggub, where you will find a key vault. You will be advised of the combination code in advance of your stay. Please be very careful to avoid locking yourself out of the house; if you are heading out do please replace the key inside the key vault. It is not difficult to pull the door shut with the key inside the house!"
                              "\n\nWifi username: TheBeachHouse"
                              "\n\nWiFi password:"
                              "\n\nChillderness1"
                              "\n\nThe lights for the house are conventionally-switched and pretty much where you would expect to find them. The layout of the house, on the other hand, is less than conventional. This is a late 1960s upside-down house, and at the time it was quite avant garde. Even today it tends to bamboozle visitors with its downstairs bedrooms. Head down from the ‘solarium’ at the top of the house to the kitchen, living, dining, cloakroom and the Captain’s Cabin bedroom."
                              "\n\nA second flight of stairs heads down to a further double bedroom, with slate shower room opposite, and the four poster en-suite master bedroom. Both of these bedrooms give onto the deck with outside dining table and over-sized sun lounger, and the fabulous view over Saundersfoot harbour and St. Brides Bay."
                              "\n\nHeating and hot water: both are controlled from the white wall-mounted box above the washing machine (in the utility room). The hot water is usually set to the AUTO programme so that water is heated morning and evening. If you require hot water beyond these parameters, simply slide the control inside the box to CONT(inuous).  The heating (through low level convection radiators and towel heaters) is also controlled from this white unit, as wells being temperature controllable using the 2 thermostats on the middle and lower floors. Make sure immersion is switched off if you are having issues (as it can affect the cold water too)"
                              "\n\nThe television and DVD player are standard fare and hopefully self-explanatory. There is a small selection of DVDs in the house. The sound system is operated via Bluetooth from a mobile ‘phone."
                              "\nPlease be aware that the walls between the houses are not the thickest and sound therefore travels. We respectfully ask guests to be considerate of our neighbours, especially late in the evening."
                              "\n\nThere is also a selection of books for you to read during your stay with us, as well as playing cards, etc."
                              "\n\nRubbish and recycling, appropriately-bagged, should be taken to the bin shelter attached to the left hand end of the block of flats (to the right of garages 1-6)."
                              "\n\nOn the day of departure, please vacate the house by 11 am at the latest to allow the housekeepers sufficient time to thoroughly prepare for our next guests. Kindly put the front door key inside the key vault and scramble the combination."
                              "\n\nWe hope you enjoy your stay. Leave us a little review if you had fun and tag your favourite photos on insta with #chilldernessmoments"
                              "\n\nLove from us all at Chillderness HQ"


//                            style: TextStyle(
//                              fontSize: 22,
//                            color: Colors.black
//                            )

                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 30,),
            //
          ],
        ),),
      ),
    );
  }
}
