import 'package:Chillderness/red-kit-green/beginingNavigation.dart';
import 'package:Chillderness/res/color.dart';
import 'package:flutter/material.dart';
import 'package:Chillderness/res/size.dart';
import 'package:Chillderness/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
class beachHouseLatenightarrivals extends StatefulWidget {
  @override
  _beachHouseLatenightarrivalsState createState() => _beachHouseLatenightarrivalsState();
}

class _beachHouseLatenightarrivalsState extends State<beachHouseLatenightarrivals> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.arrow_back_ios,
                      color: Colors.black,),
                  ),
                  Image.asset("assets/icons/logo.png",
                    //width: size.convertWidth(context, 116),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          text: "Late night arrivals",
                          style: styles.PlayfairDisplayBoldItalic(
                              fontSize: size.convert(context, 19)
                          )
                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: size.convert(context, 4),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: "At the Llareggub Beach House",
                        style: styles.PlayfairDisplayBlack(
                            fontSize: size.convert(context, 12)
                        )
                    ),
                  ),
                ],
              ),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Image.asset("assets/icons/beachHouse/Latenightarrivals.png",
              ),),
            SizedBox(height: 10,),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                        style: styles.QuicksandBold(
                            fontSize: 16,
                            color: Colors.black
                        ),
                          text: "When you are only 5-10 minutes away you can smell the high quality MSG of the Dragon Palace. Really good value, old-fashioned and unpretentious Chinese. Open until late11 ) for sit in [cheerful] take away or delivery. Dragon Palace, Pentelpoir: place your order by telephone as you leave the M4 and collect on your way into Saundersfoot: 01834 812 483. Dragon-palace.co.uk"
                                  "\n\nLate in the afternoon you could try the Roast Office in Pembroke Dock or if you don’t want to cook Sunday lunch they do a take away one for about 20 pounds for 4. It’s quite a practical solution - you can choose which meat you want and they do very bright and imaginative puddings . Visit Pembroke castle on the way ."
                          "\n\nSHOPS :"
                          "\nLocal Tesco in Saundersfoot Village is open 7/7 until 11pm but is quite uninspiring. OK for staples, and booze, but a little limited and no cold champagne darling… so think ahead if you are there for a celebration. Few or no herbs so if you manage to find a pot to plant in the garden and help us grow a herb garden this would be much appreciated."
                          "\n\nIt will tide you over until the morning when you can go to the mid-sized Co-Op in Tenby [10 minutes] and Sainsbury’s, or Haverfordwest where you have a very good Morrisons, Aldi and others. Morrisons is recommended for more imaginative products.  Carmarthen or Pembroke Dock."
                          "\n\nAt New Hedges roundabout and right, along the A478,  there is the Four Seasons farm shop on the road from Saundersfoot to Kilgetty.  That has a lot of variety, from  Kettles Crisps to nice local veg and fruit, flowers and olives, Pembrokeshire new potatoes and fresh local crab in season. Open 7am - 6.15 SA699DS"
                          "\n\nThere is another one called Priory Farm shop - not yet by us but it’s post code is SA708TN. Would love to hear your views."
                          "\n\nActually at New Hedges, McColl’s is a newsagent, Post Office and convenience store open 6am to 10pm."
                          
//                            style: TextStyle(
//                              fontSize: 22,
//                            color: Colors.black
//                            )

                      ),
                    ),
                  ),
                ],
              ),),
            SizedBox(height: 30,),
        //
          ],
        ),),
      ),
    );
  }
}
